Changelog
==========

All notable changes to this project will be documented in this file.
This project adheres to [Semantic Versioning](http://semver.org/).

Unreleased
**********
* Adds PointedMosaicParameters to SBDefinition `__init__.py` to allow importing at SBDefinition level

17.1.1

**********
* Renames `target_id` in `Beam` to  `beam_name` for consistency

17.1.0

**********
* Adds PointedMosaicParameters pointing pattern for mosaics consisting of individual pointings.
* Adds 'epoch' field to EquatorialCoordinates and EquatorialCoordinatesPST for compatibility with ADR-63.
* Adds `AltAzCoordinates`, `SpecialCoordinates` and `AltAzCoordinates` to align with ADR-63.
* Deprecates `HorizontalCoordinates`, `EquatorialCoordinates`, `EquatorialCoordinatesPST` and `SolarSystemObject`.
* Adds `target_id` field to `Beam` class in preparation for future removal of `EquatorialCoordinatesPST`
* Refactored the Coordinates to have a `to_sky_coord` method rather than storing a `_coord` object. Not technically breaking as it is a private field.
* Changed the input type of coordinate units to accept only a Tuple and not a list or string.
* Added GalacticCoordinates object
* Deprecate number_of_channels in the MCCS part of the SBDefiniton, as this information can be derived from the CSP set up.


17.0.1

**********

* Added default dish ids to MID imaging SB builder

17.0.0

**********

* Added 'name' to targets and csp_configurations, to be used as a user-friendly way to identify the entity. The existing id fields can now be set to
  a uid in the UI
* [Breaking]: In the SBDefinition, remove dish configuration and refactor `dish_allocation` to contain `dish_ids`, `dish_allocation_id` and `selected_subarray_definition`
* [Breaking]: In the SBDefinition, refactor `mccs_allocation` to contain `mccs_allocation_id` instead of `mccs_config_id`and `selected_subarray_definition` instead of `configuration`
* [Breaking]: Remove telescope from Project


16.2.1

**********

* Changed `midcbf.subband` kwarg `frequency_slice_offset` to be mandatory and defaulting to zero

16.2.0

**********

* Added `builders` module providing high-level functions for generating Mid and Low SBDefinitions.


16.1.0

**********

* Added :
    - `robust` and `spectral_averaging` to observation details level
* Removed:
    - `spectral_averaging` remoed from low array details


16.0.1

**********

* Changed typo "suppied_type" to "supplied_type" in "Proposal.ObservationSets.Supplied"

16.0.0

**********

* Changed `sub_type` to `attributes` as this reflects better the Science terminology
* Changed default of `subarray` from `AA1` to `aa1`
* Removed `SpiralParameters` from `PointingKind` enum within the Proposal
* Updated descriptions for various Proposal fields to aid in clarity
* Changed `beam_size`
* Added `uploaded_pdf` to proposals documents
* Removed `ProposalTypeENUM` as will be provided by another system, replaced field to be a string
* Removed `ProposalSubType` as will be provided by another system, replaced field to be a string
* Removed `ScienceCategory` as will be provided by another system, replaced field to be a string
* Removed `ObservationType` as will be provided by another system, replaced field to be a string
* Removed `ObservingBand` as will be provided by another system, replaced field to be a string
* Updated `results` to `result_details` within the Proposal
* Added `status` to the Proposal Investigator
* Removed a number of fields from the Proposals as they are no longer required
* Added `Sensitivity` class for use in the Proposal
* Added `SynthesizedBeamSize` class for use with the `synthesized_beam_size`
* Updated `image_size` to use AstropyQuantity
* Updated `pixel_size` to use AstropyQuantity

15.4.0

**********

* Added PDM version number to entity metadata
* Added `do_pst` boolean to `LowCBFConfiguration` to indicate that the standard correlation set-up will be used for PST observing
* Changed `SubarrayBeamConfiguration` field `number_of_channels` to optional

15.3.0

**********

* Introduced entity reference version into the status entity schemas.
* Added `PointingKind.Spiral` and  `SpiralParameters` pointing pattern to support holography observations.
* Added `EquatorialCoordinatesPST` coordinate model to support tied array beam observations

15.2.0

**********

* Make a uniform style for enums in the PDM for the names and values of status entities.

15.1.0

**********

* Added `midcbf` in `CSPConfiguration`

15.0.0

**********

* [Breaking] Updated mccs_allocation to support changes in MCCS Schema
* [Breaking] Updated lowcbf within csp_configuration
* [Breaking] Pulled SubArrayLOW and SubArrayMID into _shared
* [Breaking] Renamed scan_duration to scan_duration_ms in ScanDefinition to reflect the dump value units
* [Breaking] Removed obsolete subarray_beam_configurations and target_beam_configurations from SBDefinition
* Dependency updates:
  * Astropy >=5.0.3,<7
  * Black >22.10.0

14.3.0

*****

* Moved Elevation to top level of observation
* Changed integration to integration_time for the observation_set in proposal
* Added support for non-sidereal targets known to KatPoint using reference frame 'special'

14.2.0

*****

DO NOT USE

14.1.0

*****

* Added support for Project status entity.

14.0.1

*****

* Override pipeline runner images in CI lint/test jobs.
* Remove explicit Pydantic dependencies.
* Minor dependency updates.

14.0.0

*****

* Various changes to the Proposal model, including:
  * [BREAKING] Renames `Proposal.proposal_id` to `prsl_id`
  * [BREAKING] Renames `Investigators.proposal_investigator_id` to `investigator_id`
  * [BREAKING] Renames `Proposal.proposal_info` to `information`
  * [BREAKING] Changes the shape of `observation_set.Supplied` to contain a nested `quantity` field.
  * [POSSIBLE BREAKING] A number of fields that were optional are now mandatory. Default values (e.g. `[]`) should make this safe, but your code may break if it expects a `None`
  * Standardisation of all fields that have value and unit components to use the standard 'quantity' object
  * Implementation of standard types where applicable
  * Addition of ENUM's to further improve the quality of the information stored
  * Additional sample test files and tests added to work with the updated structure, including both Draft & Submitted status variations Union's added so that information can be stored as applicable with reduced wasted content
  * Simple descriptions added to fields with a view that these will be extended, which will add in future updates
  * ID fields now use type aliases from `_shared.ids`
  * Structure extended so that results from sensitivity calculations are stored for later referencing

* Adds static type checking (enforced by Pyright) to our code linting.

13.0.1

* Remove classmethod property (deprecated in Py3.11) 'kind' from base classes PythonProcedure and PointingPatternParameters to silence warnings.

13.0.0

*****

* [BREAKING] Renames `EBStatus` to `OSOEBStatus`
* [BREAKING] Changes status history reference fields from *_id to *_ref for consistency.
* StatusHistory entities now default to a `Metadata(version=1)`

12.0.1

*****

* fix: RequestResponse.response_received_at was mandatory rather than optional.


12.0.0

*****

* Adopts [Pydantic](https://docs.pydantic.dev/latest/) for all PDM objects.
* Unifies formerly auto-generated models and hand-written models.
* Unifies models and serialisation schemas, removing all [Marshmallow](https://marshmallow.readthedocs.io) schemas.
* New 'source of truth': OpenAPI schema yaml is generated from Python code.
* `sb_definition.ScanDefinition.pointing_correction` is non-nullable, with a default value of `MAINTAIN`.
* `EquatorialCoordinates.unit` and `HorizontalCoordinates.unit` are now always a two-element tuple (array in JSON) - previously it could be a single string if both axes of the reference frame had the same units.
* Added `RadialVelocity` to `Target` object representation shared by `ProposalInfo` and `SBDefinition`.
* [BREAKING] Pydantic models do not accept positional arguments. Previously, many objects required keyword arguments, but some allowed positional.
* [BREAKING] `ResponseResponse` and `ResponseError` renamed to `ResponseWrapper` and `ErrorWrapper`.
* [BREAKING] Pydantic models enforce runtime correctness. Attempting to instantiate an object with invalid data will raise a `ValidationError`
* [BREAKING] `schemas`,`generated` and `entities` removed: Import directly from `ska_oso_pdm` or from an entity-specific subpackage e.g. `ska_oso_pdm.proposal`
* [BREAKING] `proposal.ProposalInfo.targets` is now a list of `Target` objects:`proposal.Target` and `sb_definition.Target` are now a single, unified representation.
* [BREAKING] Many field names changed to add `_ref` suffix to distinguish between inline objects and references to IDs of other objects.
* [POSSIBLE BREAKING] Datetime strings in JSON use `Z` (Zulu time) instead of `+00:00` to indicate UTC. These are equivalent under ISO 8601.
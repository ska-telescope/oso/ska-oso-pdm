#!/usr/bin/env python3

from os import environ
from pathlib import Path

import yaml

from ska_oso_pdm import (
    OSOEBStatusHistory,
    OSOExecutionBlock,
    PdmObject,
    Project,
    Proposal,
    SBDefinition,
    SBDStatusHistory,
    SBInstance,
    SBIStatusHistory,
)

HERE = Path(__file__).resolve().parent
OPENAPI_VERSION = environ.get("OPENAPI_VERSION", "3.1.0")

WRAPPER_FILENAME = "oso-components-openapi-v1.yaml"
TOP_LEVEL_ENTITIES = {
    "eb-jsonschema.yaml": OSOExecutionBlock,
    "prj-jsonschema.yaml": Project,
    "prsl-jsonschema.yaml": Proposal,
    "sbd-jsonschema.yaml": SBDefinition,
    "sbi-jsonschema.yaml": SBInstance,
    "sbd-status-history-jsonschema.yaml": SBDStatusHistory,
    "sbi-status-history-jsonschema.yaml": SBIStatusHistory,
    "eb-status-history-jsonschema.yaml": OSOEBStatusHistory,
}


def openapi_wrapper(version: str, components: dict[str, PdmObject]):
    openapi = {
        "openapi": version,
        "paths": {},
        "info": {
            "title": "OSO Entities",
            "description": (
                "These components are the primary definitions of the entities to be used throughout the OSO services. "
                "Model code will be generated from them which can be imported into projects. The components will also "
                "be referenced in other OpenAPI docs for OSO services which have APIs."
            ),
            "license": {
                "name": "BSD 3 Clause",
                "url": "https://opensource.org/licenses/BSD-3-Clause",
            },
            "version": "1.0.0",
        },
        "components": {},
    }
    schemas = {
        entity.__name__: {"$ref": f"{filename}#"}
        for filename, entity in components.items()
    }
    openapi["components"]["schemas"] = schemas
    return openapi


# As of OpenAPI v3.1.0, JSONSchema is a strict subset of OpenAPI
# but to be pedantic, we're writing jsonschema files here, not OpenAPI.
def write_jsonschema_yaml(filename: str, pdm_entity: PdmObject):
    with open(HERE / filename, "w", encoding="utf-8") as fh:
        yaml.dump(pdm_entity.model_json_schema(), fh, sort_keys=False)


# This is a central OpenAPI document that references each
# of the schemas for the top level entities.
def write_openapi_wrapper(filename, version, components):
    openapi_parent = openapi_wrapper(version, components)
    with open(HERE / filename, "w", encoding="utf-8") as fh:
        yaml.dump(openapi_parent, fh, sort_keys=False)


def main():
    for filename, pdm_entity in TOP_LEVEL_ENTITIES.items():
        write_jsonschema_yaml(filename, pdm_entity)
    write_openapi_wrapper(WRAPPER_FILENAME, OPENAPI_VERSION, TOP_LEVEL_ENTITIES)


if __name__ == "__main__":
    main()

"""
a simple Python representation of the SBD, SBI, Project and EB status history
"""

from typing import Optional

from pydantic import Field

from ska_oso_pdm._shared import (
    ExecutionBlockID,
    ProjectID,
    SBDefinitionID,
    SBInstanceID,
)
from ska_oso_pdm._shared.atoms import TerseStrEnum
from ska_oso_pdm._shared.metadata import Metadata
from ska_oso_pdm._shared.pdm_object import PdmObject


class SBDStatus(TerseStrEnum):
    DRAFT = "Draft"
    SUBMITTED = "Submitted"
    READY = "Ready"
    IN_PROGRESS = "In Progress"
    OBSERVED = "Observed"
    SUSPENDED = "Suspended"
    FAILED_PROCESSING = "Failed Processing"
    COMPLETE = "Complete"


class SBIStatus(TerseStrEnum):
    CREATED = "Created"
    EXECUTING = "Executing"
    OBSERVED = "Observed"
    FAILED = "Failed"


class OSOEBStatus(TerseStrEnum):
    CREATED = "Created"
    FULLY_OBSERVED = "Fully Observed"
    FAILED = "Failed"


class ProjectStatus(TerseStrEnum):
    DRAFT = "Draft"
    SUBMITTED = "Submitted"
    READY = "Ready"
    IN_PROGRESS = "In Progress"
    OBSERVED = "Observed"
    COMPLETE = "Complete"
    CANCELLED = "Cancelled"
    OUT_OF_TIME = "Out of Time"


class EntityStatus(PdmObject):
    metadata: Metadata = Field(default_factory=Metadata)


class SBDStatusHistory(EntityStatus):

    sbd_ref: SBDefinitionID
    sbd_version: int = 1
    current_status: SBDStatus = SBDStatus.DRAFT
    previous_status: Optional[SBDStatus] = SBDStatus.DRAFT


class SBIStatusHistory(EntityStatus):

    sbi_ref: SBInstanceID
    sbi_version: int = 1
    current_status: SBIStatus = SBIStatus.CREATED
    previous_status: Optional[SBIStatus] = SBIStatus.CREATED


class OSOEBStatusHistory(EntityStatus):

    eb_ref: ExecutionBlockID
    eb_version: int = 1
    current_status: OSOEBStatus = OSOEBStatus.CREATED
    previous_status: Optional[OSOEBStatus] = OSOEBStatus.CREATED


class ProjectStatusHistory(EntityStatus):

    prj_ref: ProjectID
    prj_version: int = 1
    current_status: ProjectStatus = ProjectStatus.DRAFT
    previous_status: Optional[ProjectStatus] = ProjectStatus.DRAFT

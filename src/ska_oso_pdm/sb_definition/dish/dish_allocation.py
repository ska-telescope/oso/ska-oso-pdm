"""
The messages module provides simple Python representations of the structured
request and response for the TMC CentralNode.AssignResources command.
"""

from typing import Optional

from ska_oso_pdm._shared import DishAllocationID, PdmObject

__all__ = ["DishAllocation"]


class DishAllocation(PdmObject):
    """
    Represents the required dish array for an observation described by an SBDefinition

    :param dish_allocation_id: The identifier of this object within the SBDefinition
    :param selected_subarray_definition: The subarray definition, e.g. AA1. This could be inferred from the
        dish_ids but is stored here as well for UI purposes.
    :param dish_ids: The identifier of the dishes in the allocation, e.g. SKA063
    """

    dish_allocation_id: Optional[DishAllocationID] = None
    selected_subarray_definition: Optional[str] = None
    dish_ids: frozenset[str] = frozenset()

"""
The ska_oso_pdm.sb_definition_sb_definition module defines
a simple Python representation of the scheduling block
that contains the details of the observation
"""

from typing import Dict, List, Optional

from pydantic import Field

from ska_oso_pdm._shared import (
    Metadata,
    PdmObject,
    ProjectID,
    SBDefinitionID,
    ScanDefinitionID,
    Target,
    TelescopeType,
)

from .csp.csp_configuration import CSPConfiguration
from .dish.dish_allocation import DishAllocation
from .mccs.mccs_allocation import MCCSAllocation
from .procedures import ProcedureUnion
from .scan_definition import ScanDefinition
from .sdp.sdp_configuration import SDPConfiguration

__all__ = ["SBDefinition"]

# URI of the Scheduling Block definition. Not currently part of telescope model.
SBD_SCHEMA_URI = "https://schema.skao.int/ska-oso-pdm-sbd/0.1"


class SBDefinition(PdmObject):
    """
    SKA scheduling block
    """

    interface: Optional[str] = SBD_SCHEMA_URI
    sbd_id: Optional[SBDefinitionID] = None
    name: Optional[str] = None
    description: Optional[str] = None
    telescope: Optional[TelescopeType] = None
    metadata: Optional[Metadata] = None
    prj_ref: Optional[ProjectID] = None
    activities: Optional[Dict[str, ProcedureUnion]] = Field(default_factory=dict)
    targets: Optional[List[Target]] = Field(default_factory=list)
    scan_definitions: Optional[List[ScanDefinition]] = Field(default_factory=list)
    scan_sequence: Optional[List[ScanDefinitionID]] = Field(default_factory=list)
    sdp_configuration: Optional[SDPConfiguration] = None
    csp_configurations: Optional[List[CSPConfiguration]] = Field(default_factory=list)
    dish_allocations: Optional[DishAllocation] = None
    mccs_allocation: Optional[MCCSAllocation] = None

"""
The procedures module defines a representation of the
procedures listed in the activities of the SKA scheduling block.
"""

from abc import ABC
from typing import Literal, Optional, Union

from pydantic import Field
from typing_extensions import Annotated

from ska_oso_pdm._shared import PdmObject, PythonArguments, TerseStrEnum

__all__ = ["ProcedureUnion", "InlineScript", "FilesystemScript", "GitScript"]


class ScriptKind(TerseStrEnum):
    INLINE = "inline"
    FILESYSTEM = "filesystem"
    GIT = "git"


class PythonProcedure(PdmObject, ABC):
    """
    Represents a PythonProcedure to be run as an activity in an SKA scheduling block.
    """

    function_args: dict[str, PythonArguments] = Field(default_factory=dict)


class InlineScript(PythonProcedure):
    """
    Represents an InlineScript to be ran as an activity in an SKA scheduling block.
    """

    kind: Literal[ScriptKind.INLINE] = ScriptKind.INLINE
    content: str


class FilesystemScript(PythonProcedure):
    """
    Represents an FilesystemScript to be run as an activity in an SKA scheduling block.
    """

    kind: Literal[ScriptKind.FILESYSTEM] = ScriptKind.FILESYSTEM
    path: str


class GitScript(PythonProcedure):
    """
    Represents an GitScript to be run as an activity in an SKA scheduling block.
    """

    kind: Literal[ScriptKind.GIT] = ScriptKind.GIT
    repo: str
    path: str
    branch: Optional[str] = None
    commit: Optional[str] = None


ProcedureUnion = Annotated[
    Union[InlineScript, FilesystemScript, GitScript], Field(discriminator="kind")
]

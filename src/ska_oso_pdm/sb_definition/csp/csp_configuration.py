"""
The ska_oso_pdm.sb_definition.csp.csp_configuration module defines
a simple Python representation of CSP configurations.
"""

from typing import Optional

from ska_oso_pdm._shared import CSPConfigurationID, PdmObject

from .cbf import CBFConfiguration
from .common import CommonConfiguration
from .lowcbf import LowCBFConfiguration
from .midcbf import MidCBFConfiguration
from .pss import PSSConfiguration
from .pst import PSTConfiguration
from .subarray import SubarrayConfiguration

__all__ = ["CSPConfiguration"]


class CSPConfiguration(PdmObject):
    """
    Class to hold all CSP configuration.

    :param config_id: an ID for CSP configuration
    :param name: a user-friendly name to identify the CSP configuration
    :param subarray_config: Sub-array configuration to set
    :param common_config: the common CSP elements to set
    :param cbf_config: the CBF configurations to set
    :param midcbf_config: the MidCBF configurations to set
    :param lowcbf_config: the LowCBF configurations to set
    :param pst_config: the PST configurations to set
    :param pss_config: the PSS configurations to set
    """

    config_id: Optional[CSPConfigurationID] = None
    name: str = ""
    subarray: Optional[SubarrayConfiguration] = None
    common: Optional[CommonConfiguration] = None
    cbf: Optional[CBFConfiguration] = None
    midcbf: Optional[MidCBFConfiguration] = None
    lowcbf: Optional[LowCBFConfiguration] = None
    pst: Optional[PSTConfiguration] = None
    pss: Optional[PSSConfiguration] = None

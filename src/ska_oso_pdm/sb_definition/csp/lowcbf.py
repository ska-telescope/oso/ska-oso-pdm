"""
The ska_oso_pdm.sb_definition.csp.lowcbf module defines
a Python representation of the LOW CBF configuration
"""

from datetime import timedelta
from typing import List

from pydantic import Field, field_validator

from ska_oso_pdm._shared import PdmObject, TimedeltaMs

__all__ = ["Correlation", "LowCBFConfiguration"]


class Correlation(PdmObject):
    """
    Class to hold the spectral windows that will be produced by the correlator".

    :param spw_id: Integer identifying the spectral window within this CSP setup
    :param logical_fsp_ids: Frequency slice processors that will be used to produce this spectral window.
    :param zoom_factor: Integer (0-7) defining the zoom window to use. '0' means non-zoom i.e. standard continuum.
    :param centre_frequency: Centre frequency of the spectral window
    :param number_of_channels: Number of channels that the correlator will produce
    :param integration_time_ms: Averaging time after which the correlator will output a visibility
    """

    spw_id: int = Field(
        description="Integer identifying the spectral window within this CSP setup"
    )
    logical_fsp_ids: List[int] = Field(
        description="Frequency slice processors that will be used to produce this spectral window"
    )
    zoom_factor: int = Field(
        ge=0,
        le=7,
        description="Integer (0-7) defining the zoom window to use. '0' means non-zoom i.e. standard continuum",
    )
    centre_frequency: float = Field(
        description="Centre frequency of the spectral window"
    )
    number_of_channels: int = Field(
        description="Number of channels that the correlator will produce"
    )
    integration_time_ms: TimedeltaMs = Field(
        description="Averaging time in ms after which the correlator will output a visibility"
    )

    @field_validator("integration_time_ms")
    @classmethod
    def integration_time_is_valid_value(cls, integration_time):
        valid_times = (timedelta(milliseconds=283), timedelta(milliseconds=849))
        if integration_time not in valid_times:
            raise ValueError(
                "Integration time provided is not a valid option, should be 283ms or 849ms"
            )
        return integration_time


class LowCBFConfiguration(PdmObject):
    """
    Class to hold the LOW CBF configuration specification.

    :param do_pst: Boolean indicating that pulsar-timing observations are to be performed.
    :param correlation_spws: List of Correlation objects
    """

    do_pst: bool = Field(
        default=False,
        description="Boolean indicating that pulsar-timing observations are to be performed",
    )
    correlation_spws: List[Correlation] = Field(
        description="List of Correlation objects"
    )

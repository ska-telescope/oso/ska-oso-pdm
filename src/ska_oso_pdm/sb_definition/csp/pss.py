"""
The ska_oso_pdm.sb_definition.csp.pss module defines a Python
representation of a PSS configurations.
"""

from ska_oso_pdm._shared import PdmObject

__all__ = ["PSSConfiguration"]


class PSSConfiguration(PdmObject):
    """
    Class to hold PSS configurations.
    """

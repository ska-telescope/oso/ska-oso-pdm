from typing import Optional

from pydantic import AwareDatetime, Field, model_validator
from typing_extensions import Self

from ska_oso_pdm._shared import Metadata, PdmObject, ProposalID
from ska_oso_pdm._shared.atoms import TerseStrEnum
from ska_oso_pdm._shared.ids import InvestigatorID

from .info import Info


class ProposalStatus(TerseStrEnum):
    ACCEPTED = "accepted"
    DRAFT = "draft"
    REJECTED = "rejected"
    SUBMITTED = "submitted"
    UNDER_REVIEW = "under review"
    WITHDRAWN = "withdrawn"


class Proposal(PdmObject):
    """Proposal: Represents an astronomy proposal."""

    prsl_id: Optional[ProposalID] = Field(
        description="Alphanumeric unique identifier of the proposal.",
    )
    status: ProposalStatus = ProposalStatus.DRAFT
    submitted_on: Optional[AwareDatetime] = Field(
        default=None,
        description="The date and time the proposal was submitted.",
    )
    submitted_by: Optional[InvestigatorID] = Field(
        default=None, description="User id of the user that submitted the proposal"
    )
    investigator_refs: list[InvestigatorID] = Field(
        default_factory=list,
        description="IDs of the investigators that have accepted involvement in the proposal.",
    )
    metadata: Metadata = Field(
        default_factory=Metadata, description="The metadata of this Proposal."
    )
    cycle: str = Field(
        description="The cycle to which validation standards are applied"
    )
    info: Info = Field(
        # default_factory=Info, - Info needs to have all fields with defaults for this
        description="The information of the Proposal."
    )

    @model_validator(mode="after")
    def submitted_xor(self) -> Self:
        if not (bool(self.submitted_by) == bool(self.submitted_on)):
            raise ValueError(
                "'submitted_by' and 'submitted_on' must either both be set, or neither."
            )
        return self

from typing import Optional

from pydantic import EmailStr, Field

from ska_oso_pdm._shared import PdmObject
from ska_oso_pdm._shared.ids import InvestigatorID


class Investigator(PdmObject):
    investigator_id: InvestigatorID = Field(
        description="Unique identifier for the Proposal investigators class"
    )
    given_name: str = Field(description="First/fore name of the investigator")
    family_name: str = Field(description="family name of the investigator")
    email: EmailStr = Field(description="Email address for the investigator")
    organization: Optional[str] = Field(
        None, description="Organization to which the investigator belongs"
    )
    for_phd: bool = Field(
        default=False, description="Is this to count towards a qualification"
    )
    status: str = Field(description="Status of the invitation sent to co-investigators")
    principal_investigator: bool = Field(
        default=False, description="Is this a principle investigator for the proposal"
    )

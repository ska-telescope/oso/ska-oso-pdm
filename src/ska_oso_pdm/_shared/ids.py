import re
from typing import Annotated, Callable, TypeAlias, TypeVar

from pydantic.functional_validators import AfterValidator

from .atoms import TerseStrEnum


# These should match SKUID's EntityType enum
class SkuidType(TerseStrEnum):
    SBD = "sbd"
    SBI = "sbi"
    PRJ = "opj"
    PRSL = "opp"
    PB = "pb"
    EB = "eb"


_valid_types = "|".join(member.value for member in SkuidType)
SKUID_REGEX = re.compile(
    r"""
    ^(?P<type>%s)- # Specifies the type of identifier
    (?P<generatorID>[a-z0-9]+)- # Identifies the SKUID generator instance
    (?P<datetime>\d{8})- # Date 20240308 (assumed to be UTC)
    (?P<local>\d+)$ # Locally maintained sequence
    """
    % _valid_types,
    re.X,
)


def enforce_skuid_format(value: str) -> str:
    # We aren't enforcing this yet ~bjmc Apr 2024
    return value
    # assert SKUID_REGEX.match(value)
    # return value


T = TypeVar("T")


def boolean_validate(func: Callable[[T], bool]) -> Callable[[T], T]:
    def closure(value: T) -> T:
        assert func(value)
        return value

    return closure


SKUID = Annotated[str, AfterValidator(enforce_skuid_format)]
GenericID = str


# SBDefinitionID = Annotated[SKUID, AfterValidator(boolean_validate(lambda v: v.startswith(SkuidType.SBD)))]
SBDefinitionID: TypeAlias = SKUID
ProcessingBlockID: TypeAlias = SKUID
ProjectID: TypeAlias = SKUID
ProposalID: TypeAlias = SKUID
InvestigatorID: TypeAlias = GenericID
ObservationSetID: TypeAlias = GenericID
DataProductSdpID: TypeAlias = GenericID
DataProductSrcID: TypeAlias = GenericID
DocumentID: TypeAlias = GenericID
SBInstanceID: TypeAlias = SKUID
ExecutionBlockID: TypeAlias = SKUID

BeamID: TypeAlias = GenericID
ChannelsID: TypeAlias = GenericID
CSPConfigurationID: TypeAlias = GenericID
DishAllocationID: TypeAlias = GenericID
PolarisationID: TypeAlias = GenericID
ScanDefinitionID: TypeAlias = GenericID
ScanTypeID: TypeAlias = GenericID
SpectralWindowID: TypeAlias = GenericID
SubarrayBeamConfigurationID: TypeAlias = GenericID
TargetBeamConfigurationID: TypeAlias = GenericID
TargetID: TypeAlias = GenericID
MCCSAllocationID: TypeAlias = GenericID

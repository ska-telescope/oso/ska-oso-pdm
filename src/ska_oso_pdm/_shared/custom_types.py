from datetime import timedelta
from typing import Annotated, Any, Callable, TypeVar

from astropy import units
from pydantic import (
    BaseModel,
    BeforeValidator,
    ConfigDict,
    Field,
    GetCoreSchemaHandler,
    PlainSerializer,
    model_serializer,
    model_validator,
)
from pydantic.functional_validators import ModelWrapValidatorHandler
from pydantic_core import core_schema
from typing_extensions import Self

from .atoms import TerseStrEnum


def _validate_milliseconds(val: Any):
    try:
        return val if isinstance(val, timedelta) else timedelta(milliseconds=val)
    except TypeError as err:
        raise AssertionError(err) from err


TimedeltaMs = Annotated[
    timedelta,
    BeforeValidator(_validate_milliseconds),
    # https://stackoverflow.com/a/77381586/845210
    PlainSerializer(
        lambda td: float(td / timedelta(milliseconds=1)), return_type=float
    ),
    # We require our durations to be positive-valued.
    Field(gt=timedelta(0)),  # type: ignore - see https://github.com/pydantic/pydantic/issues/9472
]


class _UnitAnnotations:
    @classmethod
    def __get_pydantic_core_schema__(
        cls,
        _source_type: Any,
        _handler: GetCoreSchemaHandler,
    ) -> core_schema.CoreSchema:
        from_str_schema = core_schema.chain_schema(
            [
                core_schema.str_schema(),
                core_schema.no_info_plain_validator_function(units.Unit),
            ]
        )

        return core_schema.json_or_python_schema(
            json_schema=from_str_schema,
            python_schema=core_schema.union_schema(
                [
                    core_schema.is_instance_schema(units.UnitBase),
                    from_str_schema,
                ]
            ),
            serialization=core_schema.plain_serializer_function_ser_schema(str),
        )


AstropyUnit = Annotated[units.Unit, _UnitAnnotations]


T = TypeVar("T")


class UnitHelpers:
    """
    Convenience functions to make it easier to implement Quantity
    types that restrict the choice of units.

    Usage example:

        from astropy import units
        from pydantic import AfterValidator, WithJsonSchema
        from ska_oso_pdm._shared import AstropyUnit, AstropyQuantity

        class Allowed(TerseStrEnum):
            METERS = 'm'
            KILOMETERS = 'km'

        ConstrainedUnitType = Annotated[
            AstropyUnit,
            AfterValidator(UnitHelpers.constrain_unit_to(Allowed)),
            WithJsonSchema(UnitHelpers.enum_jsonschema(Allowed)),
        ]

        class ConstrainedQuantity(AstropyQuantity):
            unit: ConstrainedUnitType

        ConstrainedQuantityType = Annotated[
            AstropyQuantity,
            ConstrainedQuantity,
            AfterValidator(UnitHelpers.constrain_unit_to(Allowed))
        ]

        class NewModel(PdmObject):
            quantity: ConstrainedQuantityType: units.Quantity(value=0, unit=Allowed.METERS)
    """

    @staticmethod
    def constrain_unit_to(enum_type: type[TerseStrEnum]) -> Callable[[T], T]:
        def _closure(value: T) -> T:
            unit = getattr(value, "unit", value)
            # Gets enum member by value, throws exception if missing:
            enum_type(str(unit))
            return value

        return _closure

    @staticmethod
    def enum_jsonschema(enum_type: type[TerseStrEnum]) -> dict[str, str | list[str]]:
        return {"type": "string", "enum": [m.value for m in enum_type]}


class Quantity(BaseModel):
    model_config = ConfigDict(json_schema_extra={"title": "Quantity"})
    value: float
    unit: AstropyUnit = Field(
        default_factory=units.Unit
    )  # Unit() will return a dimensionless unit

    @model_validator(mode="wrap")  # type: ignore - https://github.com/pydantic/pydantic/discussions/9489
    @classmethod
    def _validate(
        cls, value: Any, handler: ModelWrapValidatorHandler[Self]
    ) -> units.Quantity:
        if isinstance(value, units.Quantity):
            return value

        validated = handler(value)
        return units.Quantity(value=validated.value, unit=validated.unit)

    @model_serializer
    def _serialize(self) -> dict[str, float | str]:
        if self.unit == units.dimensionless_unscaled:
            return dict(value=self.value)
        return dict(value=self.value, unit=str(self.unit))


AstropyQuantity = Annotated[units.Quantity, Quantity]

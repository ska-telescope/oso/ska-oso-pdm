import functools
from datetime import timedelta

from ska_oso_pdm.sb_definition import ScanDefinition

DEFAULT_SCAN_DURATION = 60000
SCAN_DEFINITION_ID_BASE = "science"

ScanDefinitionBuilder = functools.partial(
    ScanDefinition,
    scan_definition_id=SCAN_DEFINITION_ID_BASE + "1",
    scan_duration_ms=timedelta(milliseconds=DEFAULT_SCAN_DURATION),
)

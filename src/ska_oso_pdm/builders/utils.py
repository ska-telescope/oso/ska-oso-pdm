from datetime import timedelta
from random import randint
from typing import List, Optional

from ska_oso_pdm.sb_definition import SBDefinition, ScanDefinition

DEFAULT_SCAN_DURATION = 60000.0
SCAN_DEFINITION_ID_BASE = "science"
# Dish and MCCS allocation is currently an object rather than a list, so using a static ID
# is fine as there will only be one within the SBDefinition
DISH_ALLOCATION_ID = "dish-allocation-74519"
MCCS_ALLOCATION_ID = "mccs-allocation-74519"


def id_key():
    # Returns a random 5 digit number
    return randint(10000, 99999)


def target_id():
    return f"target-{id_key()}"


def csp_configuration_id():
    return f"csp-configuration-{id_key()}"


def scan_definition_id():
    return f"scan-definition-{id_key()}"


def add_scan_for_each_target(
    sb_obj: SBDefinition, scan_durations: Optional[float | List[float]]
) -> SBDefinition:
    """
    Adds a scan definition for each target defined in the SB and includes
    it in the SB's scan sequence. If SB already defines a list of scan definitions
    function will append to the list of scan definitions and scan sequence.

    The function will return a copy of the given SBDefinition object.
    """
    sb_obj_copy = sb_obj.model_copy(deep=True)

    if sb_obj_copy.targets is not None:
        if scan_durations is None:
            scan_duration_list = [DEFAULT_SCAN_DURATION] * len(sb_obj_copy.targets)
        elif isinstance(scan_durations, list):
            scan_duration_list: list[float] = scan_durations
        else:
            scan_duration_list = [scan_durations] * len(sb_obj_copy.targets)

        if len(sb_obj_copy.targets) != len(scan_duration_list):
            raise ValueError(
                f"Number of targets and scan durations does not match, "
                f"{len(sb_obj_copy.targets)} targets were defined and {len(scan_duration_list)} scan "
                f"duration values were given."
            )

        if sb_obj_copy.scan_definitions is None:
            sb_obj_copy.scan_definitions = []
        if sb_obj_copy.scan_sequence is None:
            sb_obj_copy.scan_sequence = []

        for index, target in enumerate(sb_obj_copy.targets):
            scan_def_id = scan_definition_id()
            sb_obj_copy.scan_definitions.append(
                ScanDefinition(
                    scan_definition_id=scan_def_id,
                    target_ref=target.target_id,
                    mccs_allocation_ref=(
                        sb_obj_copy.mccs_allocation.mccs_allocation_id
                        if sb_obj_copy.mccs_allocation
                        else None
                    ),
                    dish_allocation_ref=(
                        sb_obj_copy.dish_allocations.dish_allocation_id
                        if sb_obj_copy.dish_allocations
                        else None
                    ),
                    csp_configuration_ref=(
                        sb_obj_copy.csp_configurations[0].config_id
                        if sb_obj_copy.csp_configurations
                        else None
                    ),
                    scan_duration_ms=timedelta(milliseconds=scan_duration_list[index]),
                )
            )
            sb_obj_copy.scan_sequence.append(scan_def_id)
    return sb_obj_copy

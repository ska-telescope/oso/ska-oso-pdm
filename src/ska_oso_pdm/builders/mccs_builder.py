import functools
from collections.abc import Iterator

from ska_oso_pdm.builders.utils import MCCS_ALLOCATION_ID
from ska_oso_pdm.sb_definition import MCCSAllocation, SubArrayLOW
from ska_oso_pdm.sb_definition.mccs.mccs_allocation import (
    Aperture,
    SubarrayBeamConfiguration,
)

AA05_P1_STATIONS = (345, 350, 352, 431)

ApertureBuilder = functools.partial(Aperture, substation_id=1, weighting_key="uniform")

SubarrayBeamConfigurationBuilder = functools.partial(
    SubarrayBeamConfiguration,
    subarray_beam_id=1,
)


def _get_correct_subarray_definition(stations: Iterator[str]):
    match stations:
        case (345, 350, 352, 431):
            return SubArrayLOW.AA05_ALL
        case (345, 350, 352, 355, 431, 434):
            return SubArrayLOW.AA05_ALL
        case _:
            return SubArrayLOW.CUSTOM


def _create_mccs_allocation_params(
    stations, subarray_beams=None, **additional_constructor_params
):
    # If subarray beams are defined manually, use them instead of creating each for a passed station
    if subarray_beams:
        stations = (aperture.station_id for aperture in subarray_beams[0].apertures)

    # Set selected_subarray_definition based on list of stations if not set manually
    additional_constructor_params["selected_subarray_definition"] = (
        additional_constructor_params.pop(
            "selected_subarray_definition", _get_correct_subarray_definition(stations)
        )
    )

    # Add aperture for each station if subarray beams are not defined manually
    if not subarray_beams:
        subarray_beam_configs = [
            SubarrayBeamConfigurationBuilder(
                apertures=[ApertureBuilder(station_id=station) for station in stations]
            )
        ]
        return MCCSAllocation(
            subarray_beams=subarray_beam_configs, **additional_constructor_params
        )
    else:
        return MCCSAllocation(
            subarray_beams=subarray_beams, **additional_constructor_params
        )


MCCSAllocationBuilder = functools.partial(
    _create_mccs_allocation_params,
    mccs_allocation_id=MCCS_ALLOCATION_ID,
    stations=AA05_P1_STATIONS,
)

import functools

from ska_oso_pdm.builders.utils import DISH_ALLOCATION_ID
from ska_oso_pdm.sb_definition import DishAllocation

AA05_DISHES = frozenset(["SKA001", "SKA036", "SKA063", "SKA100"])

DishAllocationBuilder = functools.partial(
    DishAllocation,
    dish_allocation_id=DISH_ALLOCATION_ID,
    selected_subarray_definition="AA0.5",
    dish_ids=AA05_DISHES,
)

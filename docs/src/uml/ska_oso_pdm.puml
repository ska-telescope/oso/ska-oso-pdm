@startuml ska_oso_pdm
!pragma useIntermediatePackages false

class ska_oso_pdm._shared.metadata.Metadata {
  version: int {static}
  created_by: Optional[str] {static}
  created_on: pydantic.types.AwareDatetime {static}
  last_modified_by: Optional[str] {static}
  last_modified_on: pydantic.types.AwareDatetime {static}
  pdm_version: Optional[str] {static}
}
class ska_oso_pdm.entity_status_history.OSOEBStatusHistory {
  eb_ref: str {static}
  eb_version:int {static}
  current_status: OSOEBStatus {static}
  previous_status: Optional[OSOEBStatus] {static}
}
class ska_oso_pdm.execution_block.OSOExecutionBlock {
  interface: Optional[str] {static}
  eb_id: Optional[str] {static}
  metadata: Optional[Metadata] {static}
  telescope: TelescopeType {static}
  sbd_ref: Optional[str] {static}
  sbd_version: Optional[int] {static}
  sbi_ref: Optional[str] {static}
  request_responses: list[RequestResponse] {static}
}
class ska_oso_pdm._shared.pdm_object.PdmObject {
}
class ska_oso_pdm.project.Project {
  interface: Optional[str] {static}
  prj_id: Optional[str] {static}
  name: Optional[str] {static}
  metadata: Optional[Metadata] {static}
  author: Optional[Author] {static}
  obs_blocks: list[ObservingBlock] {static}
}
class ska_oso_pdm.proposal.proposal.Proposal {
  prsl_id: Optional[str] {static}
  status: str {static}
  submitted_on: Optional[pydantic.types.AwareDatetime] {static}
  submitted_by: Optional[str] {static}
  investigator_refs: list[str] {static}
  metadata: Optional[Metadata] {static}
  information: Optional[Information] {static}
}
class ska_oso_pdm.entity_status_history.SBDStatusHistory {
  sbd_ref: str {static}
  sbd_version:int {static}
  current_status: SBDStatus {static}
  previous_status: Optional[SBDStatus] {static}
}
class ska_oso_pdm.sb_definition.sb_definition.SBDefinition {
  interface: Optional[str] {static}
  sbd_id: Optional[str] {static}
  name: Optional[str] {static}
  description: Optional[str] {static}
  telescope: Optional[TelescopeType] {static}
  metadata: Optional[Metadata] {static}
  prj_ref: Optional[str] {static}
  activities: Optional[Dict[str, Union[InlineScript, ]]] {static}
  targets: Optional[List[Target]] {static}
  scan_definitions: Optional[List[ScanDefinition]] {static}
  scan_sequence: Optional[List[str]] {static}
  sdp_configuration: Optional[SDPConfiguration] {static}
  csp_configurations: Optional[List[CSPConfiguration]] {static}
  dish_allocations: Optional[DishAllocation] {static}
  mccs_allocation: Optional[MCCSAllocation] {static}
  subarray_beam_configurations: Optional[List[SubarrayBeamConfiguration]] {static}
  target_beam_configurations: Optional[List[TargetBeamConfiguration]] {static}
}
class ska_oso_pdm.entity_status_history.SBIStatusHistory {
  sbi_ref: str {static}
  sbi_version:int {static}
  current_status: SBIStatus {static}
  previous_status: Optional[SBIStatus] {static}
}
class ska_oso_pdm.entity_status_history.ProjectStatusHistory {
  prj_ref: str {static}
  prj_version:int {static}
  current_status: ProjectStatus {static}
  previous_status: Optional[ProjectStatus] {static}
}
class ska_oso_pdm.sb_instance.SBInstance {
  interface: Optional[str] {static}
  sbi_id: Optional[str] {static}
  metadata: Optional[Metadata] {static}
  telescope: TelescopeType {static}
  sbd_ref: Optional[str] {static}
  sbd_version: Optional[int] {static}
  eb_ref: Optional[str] {static}
  subarray_id: Optional[int] {static}
  activities: list[ActivityCall] {static}
}
class ska_oso_pdm._shared.target.Target {
  target_id: str {static}
  name: str {static}
  pointing_pattern: PointingPattern {static}
  reference_coordinate: Union[EquatorialCoordinates, ] {static}
  radial_velocity: RadialVelocity {static}
  tied_array_beams: Optional[TiedArrayBeams] {static}
}
enum ska_oso_pdm._shared.atoms.TelescopeType {
  SKA_MID: ska_mid {static}
  SKA_LOW: ska_low {static}
  MEERKAT: MeerKAT {static}
}
enum ska_oso_pdm._shared.atoms.TerseStrEnum {
}
class ska_oso_pdm._shared.custom_types.QuantityModel {
  value: float {static}
  unit: Optional[Unit] {static}
}
class ska_oso_pdm._shared.custom_types._UnitAnnotations {
}
enum ska_oso_pdm._shared.ids.SkuidType {
  SBD: sbd {static}
  SBI: sbi {static}
  PRJ: opj {static}
  PRSL: opp {static}
  PB: pb {static}
  EB: eb {static}
}
class ska_oso_pdm._shared.python_arguments.FunctionArgs {
  function_name: Optional[str] {static}
  function_args: Optional[PythonArguments] {static}
}
class ska_oso_pdm._shared.python_arguments.PythonArguments {
  args: list {static}
  kwargs: dict {static}
}
enum ska_oso_pdm._shared.target.CoordinateKind {
  EQUATORIAL: equatorial {static}
  HORIZONTAL: horizontal {static}
  SSO: sso {static}
}
class ska_oso_pdm._shared.target.Coordinates {
  EQUALITY_TOLERANCE: ClassVar[Angle] {static}
  kind: Literal {static}
  _coord: astropy.coordinates.sky_coordinate.SkyCoord {static}
}
class ska_oso_pdm._shared.target.CrossScanParameters {
  kind: Literal[PointingKind] {static}
  offset_arcsec: float {static}
}
class ska_oso_pdm._shared.target.EquatorialCoordinates {
  kind: Literal[CoordinateKind] {static}
  ra: Union[float, str, None] {static}
  dec: Union[float, str, None] {static}
  reference_frame: Optional[EquatorialCoordinatesReferenceFrame] {static}
  unit: Union[str, List[str], Tuple[str]] {static}
}
enum ska_oso_pdm._shared.target.EquatorialCoordinatesReferenceFrame {
  ICRS: icrs {static}
  FK5: fk5 {static}
}
class ska_oso_pdm._shared.target.FivePointParameters {
  kind: Literal[PointingKind] {static}
  offset_arcsec: float {static}
}
class ska_oso_pdm._shared.target.HorizontalCoordinates {
  kind: Literal[CoordinateKind] {static}
  az: float {static}
  el: float {static}
  unit: Union[str, List[str], Tuple[str]] {static}
  reference_frame: Optional[HorizontalCoordinatesReferenceFrame] {static}
}
enum ska_oso_pdm._shared.target.HorizontalCoordinatesReferenceFrame {
  ALTAZ: altaz {static}
}
enum ska_oso_pdm._shared.target.PointingKind {
  FIVE_POINT: FivePointParameters {static}
  SINGLE_POINT: SinglePointParameters {static}
  CROSS_SCAN: CrossScanParameters {static}
  RASTER: RasterParameters {static}
  STAR_RASTER: StarRasterParameters {static}
  SPIRAL: SpiralParameters {static}
}
class ska_oso_pdm._shared.target.PointingPattern {
  active: PointingKind {static}
  parameters: list[Union[FivePointParameters, ]] {static}
}
abstract class ska_oso_pdm._shared.target.PointingPatternParameters {
}
class ska_oso_pdm._shared.target.RadialVelocity {
  quantity: Quantity {static}
  definition: RadialVelocityDefinition {static}
  reference_frame: RadialVelocityReferenceFrame {static}
  redshift: float {static}
}
enum ska_oso_pdm._shared.target.RadialVelocityDefinition {
  OPTICAL: OPTICAL {static}
  RADIO: RADIO {static}
  RELATIVISTIC: RELATIVISTIC {static}
}
class ska_oso_pdm._shared.target.RadialVelocityQuantity {
  value: float {static}
  unit: RadialVelocityUnits {static}
}
enum ska_oso_pdm._shared.target.RadialVelocityReferenceFrame {
  TOPOCENTRIC: TOPOCENTRIC {static}
  LSRK: LSRK {static}
  BARYCENTRIC: BARYCENTRIC {static}
}
enum ska_oso_pdm._shared.target.RadialVelocityUnits {
  M_PER_SEC: m / s {static}
  KM_PER_SEC: km / s {static}
}
class ska_oso_pdm._shared.target.RasterParameters {
  kind: Literal[PointingKind] {static}
  row_length_arcsec: float {static}
  row_offset_arcsec: float {static}
  n_rows: int {static}
  pa: float {static}
  unidirectional: bool {static}
}
class ska_oso_pdm._shared.target.SpiralParameters {
  kind: Literal[PointingKind] {static}
  scan_extent: Quantity {static}
  track_time: Quantity {static}
  cycle_track_time: Quantity {static}
  slow_time: Quantity {static}
  sample_time: Quantity {static}
  scan_speed: Quantity {static}
  slew_speed: Quantity {static}
  twist_factor: float {static}
  high_el_slowdown_factor: float {static}
}
class ska_oso_pdm._shared.target.SinglePointParameters {
  kind: Literal[PointingKind] {static}
  offset_x_arcsec: float {static}
  offset_y_arcsec: float {static}
}
class ska_oso_pdm._shared.target.SolarSystemObject {
  kind: Literal[CoordinateKind] {static}
  reference_frame: Literal {static}
  name: SolarSystemObjectName {static}
}
enum ska_oso_pdm._shared.target.SolarSystemObjectName {
  SUN: Sun {static}
  MOON: Moon {static}
  MERCURY: Mercury {static}
  VENUS: Venus {static}
  MARS: Mars {static}
  JUPITER: Jupiter {static}
  SATURN: Saturn {static}
  URANUS: Uranus {static}
  NEPTUNE: Neptune {static}
}
class ska_oso_pdm._shared.target.StarRasterParameters {
  kind: Literal[PointingKind] {static}
  row_length_arcsec: float {static}
  n_rows: int {static}
  row_offset_angle: float {static}
  unidirectional: bool {static}
}
class ska_oso_pdm.entity_status_history.EntityStatus {
  metadata: Metadata {static}
}
enum ska_oso_pdm.entity_status_history.OSOEBStatus {
  CREATED: Created {static}
  FULLY_OBSERVED: Fully Observed {static}
  FAILED: Failed {static}
}
enum ska_oso_pdm.entity_status_history.ProjectStatus {
  DRAFT = Draft {static}
  SUBMITTED = Submitted {static}
  READY = Ready {static}
  IN_PROGRESS = In Progress {static}
  OBSERVED = Observed {static}
  COMPLETE = Complete {static}
  CANCELLED = Cancelled {static}
  OUT_OF_TIME = Out of Time {static}
}
enum ska_oso_pdm.entity_status_history.SBDStatus {
  DRAFT: Draft {static}
  SUBMITTED: Submitted {static}
  READY: Ready {static}
  IN_PROGRESS: In Progress {static}
  OBSERVED: Observed {static}
  SUSPENDED: Suspended {static}
  FAILED_PROCESSING: Failed Processing {static}
  COMPLETE: Complete {static}
}
enum ska_oso_pdm.entity_status_history.SBIStatus {
  CREATED: Created {static}
  EXECUTING: Executing {static}
  OBSERVED: Observed {static}
  FAILED: Failed {static}
}
class ska_oso_pdm.execution_block.ErrorWrapper {
  detail: Optional[str] {static}
  stacktrace: Optional[str] {static}
}
class ska_oso_pdm.execution_block.RequestResponse {
  request: Optional[str] {static}
  request_args: Optional[PythonArguments] {static}
  status: Optional[str] {static}
  response: Optional[ResponseWrapper] {static}
  error: Optional[ErrorWrapper] {static}
  request_sent_at: pydantic.types.AwareDatetime {static}
  response_received_at: Optional[AwareDatetime] {static}
}
class ska_oso_pdm.execution_block.ResponseWrapper {
  result: Optional[str] {static}
}
class ska_oso_pdm.project.Author {
  pis: list[str] {static}
  cois: list[str] {static}
}
class ska_oso_pdm.project.ObservingBlock {
  obs_block_id: str {static}
  name: Optional[str] {static}
  sbd_ids: list[str] {static}
}
class ska_oso_pdm.proposal.proposal.Investigators {
  investigator_id: Optional[int] {static}
  given_name: Optional[str] {static}
  family_name: Optional[str] {static}
  email: Optional[EmailStr] {static}
  organization: Optional[str] {static}
  for_phd: bool {static}
  principal_investigator: bool {static}
}
class ska_oso_pdm.proposal.proposal.Document {
  document_id: int {static}
  link: str {static}
  type: str {static}
}
class ska_oso_pdm.proposal.proposal.Info {
  title: Optional[str] {static}
  cycle: Optional[str] {static}
  abstract: Optional[str] {static}
  proposal_type: Optional[ProposalType] {static}
  science_category: Optional[str] {static}
  targets: list[Target] {static}
  document_ref: list[Document] {static}
  investigators: list[Investigators] {static}
  observation_sets: list[ObservationSets] {static}
  data_product_sdps: list[DataProductSDP] {static}
  data_product_src_nets: list[DataProductSRC] {static}
}
class ska_oso_pdm.proposal.proposal.ProposalType {
  main_type: Optional[str] {static}
  attributes: Optional[list[str]] {static}
}
class ska_oso_pdm.sb_definition.csp.cbf.CBFConfiguration {
  fsps: List[FSPConfiguration] {static}
  vlbi: Optional[VLBIConfiguration] {static}
}
class ska_oso_pdm.sb_definition.csp.cbf.FSPConfiguration {
  fsp_id: int {static}
  function_mode: FSPFunctionMode {static}
  frequency_slice_id: int {static}
  integration_factor: int {static}
  zoom_factor: int {static}
  channel_averaging_map: list[tuple[int, int]] {static}
  output_link_map: list[tuple] {static}
  channel_offset: Optional[int] {static}
  zoom_window_tuning: Optional[int] {static}
}
enum ska_oso_pdm.sb_definition.csp.cbf.FSPFunctionMode {
  CORR: CORR {static}
  PSS_BF: PSS-BF {static}
  PST_BF: PST-BF {static}
  VLBI: VLBI {static}
}
class ska_oso_pdm.sb_definition.csp.cbf.VLBIConfiguration {
}
class ska_oso_pdm.sb_definition.csp.common.CommonConfiguration {
  subarray_id: Optional[int] {static}
  band_5_tuning: list[float] {static}
}
class ska_oso_pdm.sb_definition.csp.csp_configuration.CSPConfiguration {
  config_id: Optional[str] {static}
  name: Optional[str] {static}
  subarray: Optional[SubarrayConfiguration] {static}
  common: Optional[CommonConfiguration] {static}
  cbf: Optional[CBFConfiguration] {static}
  midcbf: Optional[MidCBFConfiguration] {static}
  lowcbf: Optional[LowCBFConfiguration] {static}
  pst: Optional[PSTConfiguration] {static}
  pss: Optional[PSSConfiguration] {static}
}
class ska_oso_pdm.sb_definition.csp.lowcbf.LowCBFConfiguration {
  stations: StationConfiguration {static}
  vis: VisConfiguration {static}
}
class ska_oso_pdm.sb_definition.csp.pss.PSSConfiguration {
}
class ska_oso_pdm.sb_definition.csp.pst.PSTConfiguration {
}
class ska_oso_pdm.sb_definition.csp.subarray.SubarrayConfiguration {
  subarray_name: str {static}
}
class ska_oso_pdm.sb_definition.csp.lowcbf.StationBeamConfiguration {
  stn_beam_id: int {static}
  freq_ids: List[int] {static}
}
class ska_oso_pdm.sb_definition.csp.lowcbf.StationBeamHost {
  sdp_channel: int {static}
  ip_addr: str {static}
}
class ska_oso_pdm.sb_definition.csp.lowcbf.StationBeamMac {
  sdp_channel: int {static}
  server_mac: str {static}
}
class ska_oso_pdm.sb_definition.csp.lowcbf.StationBeamPort {
  sdp_channel: int {static}
  udp_port: int {static}
  stride: int {static}
}
class ska_oso_pdm.sb_definition.csp.lowcbf.StationConfiguration {
  stns: List[List[int]] {static}
  stn_beams: List[StationBeamConfiguration] {static}
}
class ska_oso_pdm.sb_definition.csp.lowcbf.VisConfiguration {
  fsp: VisFSPConfiguration {static}
  stn_beams: List[VisStationBeamConfiguration] {static}
}
class ska_oso_pdm.sb_definition.csp.lowcbf.VisFSPConfiguration {
  function_mode: str {static}
  fsp_ids: List[int] {static}
}
class ska_oso_pdm.sb_definition.csp.lowcbf.VisStationBeamConfiguration {
  stn_beam_id: int {static}
  integration_ms: timedelta {static}
  host: List[StationBeamHost] {static}
  port: List[StationBeamPort] {static}
  mac: Optional[List[StationBeamMac]] {static}
}
class ska_oso_pdm.sb_definition.dish.dish_allocation.DishAllocation {
  dish_allocation_id: str {static}
  selected_subarray_definition: str {static}
  dish_ids: frozenset[str] {static}
}

enum ska_oso_pdm.sb_definition.csp.midcbf.ReceiverBand {
  BAND_1: 1 {static}
  BAND_2: 2 {static}
  BAND_5A: 5a {static}
  BAND_5B: 5b {static}
}
class ska_oso_pdm.sb_definition.mccs.mccs_allocation.MCCSAllocation {
  mccs_allocation_id: str {static}
  selected_subarray_definition: SubArrayLOW {static}
  subarray_beams: list[SubarrayBeamConfiguration] {static}
}
class ska_oso_pdm.sb_definition.procedures.FilesystemScript {
  kind: Literal[ScriptKind] {static}
  path: str {static}
}
class ska_oso_pdm.sb_definition.procedures.GitScript {
  kind: Literal[ScriptKind] {static}
  repo: str {static}
  path: str {static}
  branch: Optional[str] {static}
  commit: Optional[str] {static}
}
class ska_oso_pdm.sb_definition.procedures.InlineScript {
  kind: Literal[ScriptKind] {static}
  content: str {static}
}
abstract class ska_oso_pdm.sb_definition.procedures.PythonProcedure {
  function_args: dict[str, PythonArguments] {static}
}
enum ska_oso_pdm.sb_definition.procedures.ScriptKind {
  INLINE: inline {static}
  FILESYSTEM: filesystem {static}
  GIT: git {static}
}
class ska_oso_pdm.sb_definition.sdp.sdp_configuration.SDPConfiguration {
  execution_block: Optional[ExecutionBlock] {static}
  resources: Optional[Resources] {static}
  processing_blocks: list[ProcessingBlock] {static}
}
class ska_oso_pdm.sb_definition.scan_definition.ScanDefinition {
  scan_definition_id: Optional[str] {static}
  scan_duration: timedelta {static}
  target_ref: Optional[str] {static}
  mccs_allocation_ref: Optional[str] {static}
  target_beam_configuration_refs: List[str] {static}
  dish_allocation_ref: Optional[str] {static}
  scan_type_ref: Optional[str] {static}
  csp_configuration_ref: Optional[str] {static}
  pointing_correction: PointingCorrection {static}
}
enum ska_oso_pdm.sb_definition.scan_definition.PointingCorrection {
  MAINTAIN: MAINTAIN {static}
  UPDATE: UPDATE {static}
  RESET: RESET {static}
}
class ska_oso_pdm.sb_definition.sdp.beam.Beam {
  beam_id: str {static}
  function: BeamFunction {static}
  search_beam_id: Optional[int] {static}
  timing_beam_id: Optional[int] {static}
  vlbi_beam_id: Optional[int] {static}
}
enum ska_oso_pdm.sb_definition.sdp.beam.BeamFunction {
  VISIBILITIES: visibilities {static}
  PULSAR_SEARCH: pulsar search {static}
  PULSAR_TIMING: pulsar timing {static}
  VLBI: vlbi {static}
  TRANSIENT_BUFFER: transient buffer {static}
}
class ska_oso_pdm.sb_definition.sdp.channels.Channels {
  channels_id: str {static}
  spectral_windows: list[SpectralWindow] {static}
}
class ska_oso_pdm.sb_definition.sdp.channels.SpectralWindow {
  spectral_window_id: str {static}
  count: int {static}
  start: int {static}
  freq_min: float {static}
  freq_max: float {static}
  stride: Optional[int] {static}
  link_map: list[tuple] {static}
}
class ska_oso_pdm.sb_definition.sdp.execution_block.ExecutionBlock {
  eb_id: str {static}
  max_length: float {static}
  context: dict {static}
  beams: list[Beam] {static}
  scan_types: list[ScanType] {static}
  channels: list[Channels] {static}
  polarisations: list[Polarisation] {static}
}
class ska_oso_pdm.sb_definition.sdp.polarisation.Polarisation {
  polarisations_id: str {static}
  corr_type: list[str] {static}
}
class ska_oso_pdm.sb_definition.sdp.scan_type.ScanType {
  scan_type_id: str {static}
  derive_from: Optional[str] {static}
  beams: list[BeamMapping] {static}
}
class ska_oso_pdm.sb_definition.sdp.processing_block.PbDependency {
  pb_ref: str {static}
  kind: list[str] {static}
}
class ska_oso_pdm.sb_definition.sdp.processing_block.ProcessingBlock {
  pb_id: str {static}
  script: Script {static}
  sbi_refs: list[str] {static}
  parameters: dict {static}
  dependencies: list[PbDependency] {static}
}
class ska_oso_pdm.sb_definition.sdp.processing_block.Script {
  name: str {static}
  kind: ScriptKind {static}
  version: str {static}
}
enum ska_oso_pdm.sb_definition.sdp.processing_block.ScriptKind {
  REALTIME: realtime {static}
  BATCH: batch {static}
}
class ska_oso_pdm.sb_definition.sdp.resources.Resources {
  csp_links: list[int] {static}
  receptors: list[str] {static}
  receive_nodes: Optional[int] {static}
}
class ska_oso_pdm.sb_definition.sdp.scan_type.BeamMapping {
  beam_ref: str {static}
  field_ref: Optional[str] {static}
  channels_ref: Optional[str] {static}
  polarisations_ref: Optional[str] {static}
}
class ska_oso_pdm.sb_instance.ActivityCall {
  activity_ref: Optional[str] {static}
  executed_at: pydantic.types.AwareDatetime {static}
  runtime_args: list[FunctionArgs] {static}
}
ska_oso_pdm._shared.pdm_object.PdmObject <|-- ska_oso_pdm._shared.metadata.Metadata
ska_oso_pdm.entity_status_history.OSOEBStatusHistory *-- ska_oso_pdm.entity_status_history.OSOEBStatus
ska_oso_pdm.entity_status_history.EntityStatus <|-- ska_oso_pdm.entity_status_history.OSOEBStatusHistory
ska_oso_pdm.execution_block.OSOExecutionBlock *-- ska_oso_pdm._shared.metadata.Metadata
ska_oso_pdm.execution_block.OSOExecutionBlock *-- ska_oso_pdm._shared.atoms.TelescopeType
ska_oso_pdm.execution_block.OSOExecutionBlock *-- ska_oso_pdm.execution_block.RequestResponse
ska_oso_pdm._shared.pdm_object.PdmObject <|-- ska_oso_pdm.execution_block.OSOExecutionBlock
ska_oso_pdm.project.Project *-- ska_oso_pdm._shared.metadata.Metadata
ska_oso_pdm.project.Project *-- ska_oso_pdm._shared.atoms.TelescopeType
ska_oso_pdm.project.Project *-- ska_oso_pdm.project.Author
ska_oso_pdm.project.Project *-- ska_oso_pdm.project.ObservingBlock
ska_oso_pdm._shared.pdm_object.PdmObject <|-- ska_oso_pdm.project.Project
ska_oso_pdm.proposal.proposal.Proposal *-- ska_oso_pdm._shared.metadata.Metadata
ska_oso_pdm.proposal.proposal.Proposal *-- ska_oso_pdm.proposal.proposal.Info
ska_oso_pdm._shared.pdm_object.PdmObject <|-- ska_oso_pdm.proposal.proposal.Proposal
ska_oso_pdm.entity_status_history.SBDStatusHistory *-- ska_oso_pdm.entity_status_history.SBDStatus
ska_oso_pdm.entity_status_history.EntityStatus <|-- ska_oso_pdm.entity_status_history.SBDStatusHistory
ska_oso_pdm.sb_definition.sb_definition.SBDefinition *-- ska_oso_pdm._shared.atoms.TelescopeType
ska_oso_pdm.sb_definition.sb_definition.SBDefinition *-- ska_oso_pdm._shared.metadata.Metadata
ska_oso_pdm.sb_definition.sb_definition.SBDefinition *-- ska_oso_pdm.sb_definition.procedures.InlineScript
ska_oso_pdm.sb_definition.sb_definition.SBDefinition *-- ska_oso_pdm._shared.target.Target
ska_oso_pdm.sb_definition.sb_definition.SBDefinition *-- ska_oso_pdm.sb_definition.scan_definition.ScanDefinition
ska_oso_pdm.sb_definition.sb_definition.SBDefinition *-- ska_oso_pdm.sb_definition.sdp.sdp_configuration.SDPConfiguration
ska_oso_pdm.sb_definition.sb_definition.SBDefinition *-- ska_oso_pdm.sb_definition.csp.csp_configuration.CSPConfiguration
ska_oso_pdm.sb_definition.sb_definition.SBDefinition *-- ska_oso_pdm.sb_definition.dish.dish_allocation.DishAllocation
ska_oso_pdm.sb_definition.sb_definition.SBDefinition *-- ska_oso_pdm.sb_definition.mccs.mccs_allocation.MCCSAllocation
ska_oso_pdm._shared.pdm_object.PdmObject <|-- ska_oso_pdm.sb_definition.sb_definition.SBDefinition
ska_oso_pdm.entity_status_history.SBIStatusHistory *-- ska_oso_pdm.entity_status_history.SBIStatus
ska_oso_pdm.entity_status_history.EntityStatus <|-- ska_oso_pdm.entity_status_history.SBIStatusHistory
ska_oso_pdm.entity_status_history.ProjectStatusHistory *-- ska_oso_pdm.entity_status_history.ProjectStatus
ska_oso_pdm.entity_status_history.EntityStatus <|-- ska_oso_pdm.entity_status_history.ProjectStatusHistory
ska_oso_pdm.sb_instance.SBInstance *-- ska_oso_pdm._shared.metadata.Metadata
ska_oso_pdm.sb_instance.SBInstance *-- ska_oso_pdm._shared.atoms.TelescopeType
ska_oso_pdm.sb_instance.SBInstance *-- ska_oso_pdm.sb_instance.ActivityCall
ska_oso_pdm._shared.pdm_object.PdmObject <|-- ska_oso_pdm.sb_instance.SBInstance
ska_oso_pdm._shared.target.Target *-- ska_oso_pdm._shared.target.PointingPattern
ska_oso_pdm._shared.target.Target *-- ska_oso_pdm._shared.target.EquatorialCoordinates
ska_oso_pdm._shared.target.Target *-- ska_oso_pdm._shared.target.RadialVelocity
ska_oso_pdm._shared.pdm_object.PdmObject <|-- ska_oso_pdm._shared.target.Target
ska_oso_pdm._shared.python_arguments.FunctionArgs *-- ska_oso_pdm._shared.python_arguments.PythonArguments
ska_oso_pdm._shared.pdm_object.PdmObject <|-- ska_oso_pdm._shared.python_arguments.FunctionArgs
ska_oso_pdm._shared.pdm_object.PdmObject <|-- ska_oso_pdm._shared.python_arguments.PythonArguments
ska_oso_pdm._shared.pdm_object.PdmObject <|-- ska_oso_pdm._shared.target.Coordinates
ska_oso_pdm._shared.target.CrossScanParameters *-- ska_oso_pdm._shared.target.PointingKind
ska_oso_pdm._shared.target.PointingPatternParameters <|-- ska_oso_pdm._shared.target.CrossScanParameters
ska_oso_pdm._shared.target.EquatorialCoordinates *-- ska_oso_pdm._shared.target.CoordinateKind
ska_oso_pdm._shared.target.EquatorialCoordinates *-- ska_oso_pdm._shared.target.EquatorialCoordinatesReferenceFrame
ska_oso_pdm._shared.target.Coordinates <|-- ska_oso_pdm._shared.target.EquatorialCoordinates
ska_oso_pdm._shared.target.FivePointParameters *-- ska_oso_pdm._shared.target.PointingKind
ska_oso_pdm._shared.target.PointingPatternParameters <|-- ska_oso_pdm._shared.target.FivePointParameters
ska_oso_pdm._shared.target.HorizontalCoordinates *-- ska_oso_pdm._shared.target.CoordinateKind
ska_oso_pdm._shared.target.HorizontalCoordinates *-- ska_oso_pdm._shared.target.HorizontalCoordinatesReferenceFrame
ska_oso_pdm._shared.target.Coordinates <|-- ska_oso_pdm._shared.target.HorizontalCoordinates
ska_oso_pdm._shared.target.PointingPattern *-- ska_oso_pdm._shared.target.PointingKind
ska_oso_pdm._shared.target.PointingPattern *-- ska_oso_pdm._shared.target.FivePointParameters
ska_oso_pdm._shared.pdm_object.PdmObject <|-- ska_oso_pdm._shared.target.PointingPattern
ska_oso_pdm._shared.pdm_object.PdmObject <|-- ska_oso_pdm._shared.target.PointingPatternParameters
ska_oso_pdm._shared.target.RadialVelocity *-- ska_oso_pdm._shared.target.RadialVelocityDefinition
ska_oso_pdm._shared.target.RadialVelocity *-- ska_oso_pdm._shared.target.RadialVelocityReferenceFrame
ska_oso_pdm._shared.pdm_object.PdmObject <|-- ska_oso_pdm._shared.target.RadialVelocity
ska_oso_pdm._shared.target.RadialVelocityQuantity *-- ska_oso_pdm._shared.target.RadialVelocityUnits
ska_oso_pdm._shared.custom_types.QuantityModel <|-- ska_oso_pdm._shared.target.RadialVelocityQuantity
ska_oso_pdm._shared.target.RasterParameters *-- ska_oso_pdm._shared.target.PointingKind
ska_oso_pdm._shared.target.PointingPatternParameters <|-- ska_oso_pdm._shared.target.RasterParameters
ska_oso_pdm._shared.target.SinglePointParameters *-- ska_oso_pdm._shared.target.PointingKind
ska_oso_pdm._shared.target.PointingPatternParameters <|-- ska_oso_pdm._shared.target.SinglePointParameters
ska_oso_pdm._shared.target.SolarSystemObject *-- ska_oso_pdm._shared.target.CoordinateKind
ska_oso_pdm._shared.target.SolarSystemObject *-- ska_oso_pdm._shared.target.SolarSystemObjectName
ska_oso_pdm._shared.target.Coordinates <|-- ska_oso_pdm._shared.target.SolarSystemObject
ska_oso_pdm._shared.target.StarRasterParameters *-- ska_oso_pdm._shared.target.PointingKind
ska_oso_pdm._shared.target.PointingPatternParameters <|-- ska_oso_pdm._shared.target.StarRasterParameters
ska_oso_pdm._shared.target.SpiralParameters *-- ska_oso_pdm._shared.target.PointingKind
ska_oso_pdm._shared.target.PointingPatternParameters <|-- ska_oso_pdm._shared.target.SpiralParameters
ska_oso_pdm.entity_status_history.EntityStatus *-- ska_oso_pdm._shared.metadata.Metadata
ska_oso_pdm._shared.pdm_object.PdmObject <|-- ska_oso_pdm.entity_status_history.EntityStatus
ska_oso_pdm._shared.pdm_object.PdmObject <|-- ska_oso_pdm.execution_block.ErrorWrapper
ska_oso_pdm.execution_block.RequestResponse *-- ska_oso_pdm._shared.python_arguments.PythonArguments
ska_oso_pdm.execution_block.RequestResponse *-- ska_oso_pdm.execution_block.ResponseWrapper
ska_oso_pdm.execution_block.RequestResponse *-- ska_oso_pdm.execution_block.ErrorWrapper
ska_oso_pdm._shared.pdm_object.PdmObject <|-- ska_oso_pdm.execution_block.RequestResponse
ska_oso_pdm._shared.pdm_object.PdmObject <|-- ska_oso_pdm.execution_block.ResponseWrapper
ska_oso_pdm._shared.pdm_object.PdmObject <|-- ska_oso_pdm.project.Author
ska_oso_pdm._shared.pdm_object.PdmObject <|-- ska_oso_pdm.project.ObservingBlock
ska_oso_pdm._shared.pdm_object.PdmObject <|-- ska_oso_pdm.proposal.proposal.Investigators
ska_oso_pdm.proposal.proposal.Info *-- ska_oso_pdm.proposal.proposal.ProposalType
ska_oso_pdm.proposal.proposal.Info *-- ska_oso_pdm._shared.target.Target
ska_oso_pdm.proposal.proposal.Info *-- ska_oso_pdm.proposal.proposal.Investigators
ska_oso_pdm._shared.pdm_object.PdmObject <|-- ska_oso_pdm.proposal.proposal.Info
ska_oso_pdm._shared.pdm_object.PdmObject <|-- ska_oso_pdm.proposal.proposal.ProposalType
ska_oso_pdm.sb_definition.csp.cbf.CBFConfiguration *-- ska_oso_pdm.sb_definition.csp.cbf.FSPConfiguration
ska_oso_pdm.sb_definition.csp.cbf.CBFConfiguration *-- ska_oso_pdm.sb_definition.csp.cbf.VLBIConfiguration
ska_oso_pdm._shared.pdm_object.PdmObject <|-- ska_oso_pdm.sb_definition.csp.cbf.CBFConfiguration
ska_oso_pdm.sb_definition.csp.cbf.FSPConfiguration *-- ska_oso_pdm.sb_definition.csp.cbf.FSPFunctionMode
ska_oso_pdm._shared.pdm_object.PdmObject <|-- ska_oso_pdm.sb_definition.csp.cbf.FSPConfiguration
ska_oso_pdm._shared.pdm_object.PdmObject <|-- ska_oso_pdm.sb_definition.csp.cbf.VLBIConfiguration
ska_oso_pdm._shared.pdm_object.PdmObject <|-- ska_oso_pdm.sb_definition.csp.common.CommonConfiguration
ska_oso_pdm.sb_definition.csp.csp_configuration.CSPConfiguration *-- ska_oso_pdm.sb_definition.csp.subarray.SubarrayConfiguration
ska_oso_pdm.sb_definition.csp.csp_configuration.CSPConfiguration *-- ska_oso_pdm.sb_definition.csp.common.CommonConfiguration
ska_oso_pdm.sb_definition.csp.csp_configuration.CSPConfiguration *-- ska_oso_pdm.sb_definition.csp.cbf.CBFConfiguration
ska_oso_pdm.sb_definition.csp.csp_configuration.CSPConfiguration *-- ska_oso_pdm.sb_definition.csp.lowcbf.LowCBFConfiguration
ska_oso_pdm.sb_definition.csp.csp_configuration.CSPConfiguration *-- ska_oso_pdm.sb_definition.csp.pst.PSTConfiguration
ska_oso_pdm.sb_definition.csp.csp_configuration.CSPConfiguration *-- ska_oso_pdm.sb_definition.csp.pss.PSSConfiguration
ska_oso_pdm._shared.pdm_object.PdmObject <|-- ska_oso_pdm.sb_definition.csp.csp_configuration.CSPConfiguration
ska_oso_pdm.sb_definition.csp.lowcbf.LowCBFConfiguration *-- ska_oso_pdm.sb_definition.csp.lowcbf.StationConfiguration
ska_oso_pdm.sb_definition.csp.lowcbf.LowCBFConfiguration *-- ska_oso_pdm.sb_definition.csp.lowcbf.VisConfiguration
ska_oso_pdm._shared.pdm_object.PdmObject <|-- ska_oso_pdm.sb_definition.csp.lowcbf.LowCBFConfiguration
ska_oso_pdm._shared.pdm_object.PdmObject <|-- ska_oso_pdm.sb_definition.csp.pss.PSSConfiguration
ska_oso_pdm._shared.pdm_object.PdmObject <|-- ska_oso_pdm.sb_definition.csp.pst.PSTConfiguration
ska_oso_pdm._shared.pdm_object.PdmObject <|-- ska_oso_pdm.sb_definition.csp.subarray.SubarrayConfiguration
ska_oso_pdm._shared.pdm_object.PdmObject <|-- ska_oso_pdm.sb_definition.csp.lowcbf.StationBeamConfiguration
ska_oso_pdm._shared.pdm_object.PdmObject <|-- ska_oso_pdm.sb_definition.csp.lowcbf.StationBeamHost
ska_oso_pdm._shared.pdm_object.PdmObject <|-- ska_oso_pdm.sb_definition.csp.lowcbf.StationBeamMac
ska_oso_pdm._shared.pdm_object.PdmObject <|-- ska_oso_pdm.sb_definition.csp.lowcbf.StationBeamPort
ska_oso_pdm.sb_definition.csp.lowcbf.StationConfiguration *-- ska_oso_pdm.sb_definition.csp.lowcbf.StationBeamConfiguration
ska_oso_pdm._shared.pdm_object.PdmObject <|-- ska_oso_pdm.sb_definition.csp.lowcbf.StationConfiguration
ska_oso_pdm.sb_definition.csp.lowcbf.VisConfiguration *-- ska_oso_pdm.sb_definition.csp.lowcbf.VisFSPConfiguration
ska_oso_pdm.sb_definition.csp.lowcbf.VisConfiguration *-- ska_oso_pdm.sb_definition.csp.lowcbf.VisStationBeamConfiguration
ska_oso_pdm._shared.pdm_object.PdmObject <|-- ska_oso_pdm.sb_definition.csp.lowcbf.VisConfiguration
ska_oso_pdm._shared.pdm_object.PdmObject <|-- ska_oso_pdm.sb_definition.csp.lowcbf.VisFSPConfiguration
ska_oso_pdm.sb_definition.csp.lowcbf.VisStationBeamConfiguration *-- ska_oso_pdm.sb_definition.csp.lowcbf.StationBeamHost
ska_oso_pdm.sb_definition.csp.lowcbf.VisStationBeamConfiguration *-- ska_oso_pdm.sb_definition.csp.lowcbf.StationBeamPort
ska_oso_pdm.sb_definition.csp.lowcbf.VisStationBeamConfiguration *-- ska_oso_pdm.sb_definition.csp.lowcbf.StationBeamMac
ska_oso_pdm._shared.pdm_object.PdmObject <|-- ska_oso_pdm.sb_definition.csp.lowcbf.VisStationBeamConfiguration
ska_oso_pdm._shared.pdm_object.PdmObject <|-- ska_oso_pdm.sb_definition.dish.dish_allocation.DishAllocation
ska_oso_pdm.sb_definition.csp.midcbf.MidCBFConfiguration *-- ska_oso_pdm.sb_definition.csp.midcbf.ReceiverBand
ska_oso_pdm._shared.pdm_object.PdmObject <|-- ska_oso_pdm.sb_definition.mccs.mccs_allocation.MCCSAllocation
ska_oso_pdm.sb_definition.procedures.FilesystemScript *-- ska_oso_pdm.sb_definition.procedures.ScriptKind
ska_oso_pdm.sb_definition.procedures.PythonProcedure <|-- ska_oso_pdm.sb_definition.procedures.FilesystemScript
ska_oso_pdm.sb_definition.procedures.GitScript *-- ska_oso_pdm.sb_definition.procedures.ScriptKind
ska_oso_pdm.sb_definition.procedures.FilesystemScript <|-- ska_oso_pdm.sb_definition.procedures.GitScript
ska_oso_pdm.sb_definition.procedures.InlineScript *-- ska_oso_pdm.sb_definition.procedures.ScriptKind
ska_oso_pdm.sb_definition.procedures.PythonProcedure <|-- ska_oso_pdm.sb_definition.procedures.InlineScript
ska_oso_pdm.sb_definition.procedures.PythonProcedure *-- ska_oso_pdm._shared.python_arguments.PythonArguments
ska_oso_pdm._shared.pdm_object.PdmObject <|-- ska_oso_pdm.sb_definition.procedures.PythonProcedure
ska_oso_pdm.sb_definition.sdp.sdp_configuration.SDPConfiguration *-- ska_oso_pdm.sb_definition.sdp.execution_block.ExecutionBlock
ska_oso_pdm.sb_definition.sdp.sdp_configuration.SDPConfiguration *-- ska_oso_pdm.sb_definition.sdp.resources.Resources
ska_oso_pdm.sb_definition.sdp.sdp_configuration.SDPConfiguration *-- ska_oso_pdm.sb_definition.sdp.processing_block.ProcessingBlock
ska_oso_pdm._shared.pdm_object.PdmObject <|-- ska_oso_pdm.sb_definition.sdp.sdp_configuration.SDPConfiguration
ska_oso_pdm.sb_definition.scan_definition.ScanDefinition *-- ska_oso_pdm.sb_definition.scan_definition.PointingCorrection
ska_oso_pdm._shared.pdm_object.PdmObject <|-- ska_oso_pdm.sb_definition.scan_definition.ScanDefinition
ska_oso_pdm.sb_definition.sdp.beam.Beam *-- ska_oso_pdm.sb_definition.sdp.beam.BeamFunction
ska_oso_pdm._shared.pdm_object.PdmObject <|-- ska_oso_pdm.sb_definition.sdp.beam.Beam
ska_oso_pdm.sb_definition.sdp.channels.Channels *-- ska_oso_pdm.sb_definition.sdp.channels.SpectralWindow
ska_oso_pdm._shared.pdm_object.PdmObject <|-- ska_oso_pdm.sb_definition.sdp.channels.Channels
ska_oso_pdm._shared.pdm_object.PdmObject <|-- ska_oso_pdm.sb_definition.sdp.channels.SpectralWindow
ska_oso_pdm.sb_definition.sdp.execution_block.ExecutionBlock *-- ska_oso_pdm.sb_definition.sdp.beam.Beam
ska_oso_pdm.sb_definition.sdp.execution_block.ExecutionBlock *-- ska_oso_pdm.sb_definition.sdp.scan_type.ScanType
ska_oso_pdm.sb_definition.sdp.execution_block.ExecutionBlock *-- ska_oso_pdm.sb_definition.sdp.channels.Channels
ska_oso_pdm.sb_definition.sdp.execution_block.ExecutionBlock *-- ska_oso_pdm.sb_definition.sdp.polarisation.Polarisation
ska_oso_pdm._shared.pdm_object.PdmObject <|-- ska_oso_pdm.sb_definition.sdp.execution_block.ExecutionBlock
ska_oso_pdm._shared.pdm_object.PdmObject <|-- ska_oso_pdm.sb_definition.sdp.polarisation.Polarisation
ska_oso_pdm.sb_definition.sdp.scan_type.ScanType *-- ska_oso_pdm.sb_definition.sdp.scan_type.BeamMapping
ska_oso_pdm._shared.pdm_object.PdmObject <|-- ska_oso_pdm.sb_definition.sdp.scan_type.ScanType
ska_oso_pdm._shared.pdm_object.PdmObject <|-- ska_oso_pdm.sb_definition.sdp.processing_block.PbDependency
ska_oso_pdm.sb_definition.sdp.processing_block.ProcessingBlock *-- ska_oso_pdm.sb_definition.sdp.processing_block.Script
ska_oso_pdm.sb_definition.sdp.processing_block.ProcessingBlock *-- ska_oso_pdm.sb_definition.sdp.processing_block.PbDependency
ska_oso_pdm._shared.pdm_object.PdmObject <|-- ska_oso_pdm.sb_definition.sdp.processing_block.ProcessingBlock
ska_oso_pdm.sb_definition.sdp.processing_block.Script *-- ska_oso_pdm.sb_definition.sdp.processing_block.ScriptKind
ska_oso_pdm._shared.pdm_object.PdmObject <|-- ska_oso_pdm.sb_definition.sdp.processing_block.Script
ska_oso_pdm._shared.pdm_object.PdmObject <|-- ska_oso_pdm.sb_definition.sdp.resources.Resources
ska_oso_pdm._shared.pdm_object.PdmObject <|-- ska_oso_pdm.sb_definition.sdp.scan_type.BeamMapping
ska_oso_pdm.sb_instance.ActivityCall *-- ska_oso_pdm._shared.python_arguments.FunctionArgs
ska_oso_pdm._shared.pdm_object.PdmObject <|-- ska_oso_pdm.sb_instance.ActivityCall

@enduml

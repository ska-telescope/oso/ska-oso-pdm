@startuml ska_oso_pdm.sb_definition
!pragma useIntermediatePackages false

class ska_oso_pdm.sb_definition.csp.csp_configuration.CSPConfiguration {
  config_id: Optional[str] {static}
  subarray: Optional[SubarrayConfiguration] {static}
  common: Optional[CommonConfiguration] {static}
  cbf: Optional[CBFConfiguration] {static}
  lowcbf: Optional[LowCBFConfiguration] {static}
  pst: Optional[PSTConfiguration] {static}
  pss: Optional[PSSConfiguration] {static}
}
class ska_oso_pdm.sb_definition.dish.dish_allocation.DishAllocation {
  receptor_ids: frozenset[str] {static}
}
class ska_oso_pdm.sb_definition.dish.dish_configuration.DishConfiguration {
  dish_configuration_id: str {static}
  receiver_band: ReceiverBand {static}
}
class ska_oso_pdm.sb_definition.mccs.mccs_allocation.MCCSAllocation {
  mccs_allocation_id: str {static}
  selected_subarray_definition: SubArrayLOW {static}
  subarray_beams: list[SubarrayBeamConfiguration] {static}
}
enum ska_oso_pdm.sb_definition.scan_definition.PointingCorrection {
  MAINTAIN: MAINTAIN {static}
  UPDATE: UPDATE {static}
  RESET: RESET {static}
}
class ska_oso_pdm.sb_definition.sb_definition.SBDefinition {
  interface: Optional[str] {static}
  sbd_id: Optional[str] {static}
  name: Optional[str] {static}
  description: Optional[str] {static}
  telescope: Optional[TelescopeType] {static}
  metadata: Optional[Metadata] {static}
  prj_ref: Optional[str] {static}
  activities: Optional[Dict[str, Union[InlineScript, ]]] {static}
  targets: Optional[List[Target]] {static}
  scan_definitions: Optional[List[ScanDefinition]] {static}
  scan_sequence: Optional[List[str]] {static}
  sdp_configuration: Optional[SDPConfiguration] {static}
  dish_configurations: Optional[List[DishConfiguration]] {static}
  csp_configurations: Optional[List[CSPConfiguration]] {static}
  dish_allocations: Optional[DishAllocation] {static}
  mccs_allocation: Optional[MCCSAllocation] {static}
}
class ska_oso_pdm.sb_definition.sdp.sdp_configuration.SDPConfiguration {
  execution_block: Optional[ExecutionBlock] {static}
  resources: Optional[Resources] {static}
  processing_blocks: list[ProcessingBlock] {static}
}
class ska_oso_pdm.sb_definition.scan_definition.ScanDefinition {
  scan_definition_id: Optional[str] {static}
  scan_duration: timedelta {static}
  target_ref: Optional[str] {static}
  mccs_allocation_ref: Optional[str] {static}
  target_beam_configuration_refs: List[str] {static}
  dish_allocation_ref: Optional[str] {static}
  scan_type_ref: Optional[str] {static}
  csp_configuration_ref: Optional[str] {static}
  pointing_correction: PointingCorrection {static}
}
class ska_oso_pdm.sb_definition.csp.cbf.CBFConfiguration {
  fsps: List[FSPConfiguration] {static}
  vlbi: Optional[VLBIConfiguration] {static}
}
class ska_oso_pdm.sb_definition.csp.cbf.FSPConfiguration {
  fsp_id: int {static}
  function_mode: FSPFunctionMode {static}
  frequency_slice_id: int {static}
  integration_factor: int {static}
  zoom_factor: int {static}
  channel_averaging_map: list[tuple[int, int]] {static}
  output_link_map: list[tuple] {static}
  channel_offset: Optional[int] {static}
  zoom_window_tuning: Optional[int] {static}
}
enum ska_oso_pdm.sb_definition.csp.cbf.FSPFunctionMode {
  CORR: CORR {static}
  PSS_BF: PSS-BF {static}
  PST_BF: PST-BF {static}
  VLBI: VLBI {static}
}
class ska_oso_pdm.sb_definition.csp.cbf.VLBIConfiguration {
}
class ska_oso_pdm.sb_definition.csp.common.CommonConfiguration {
  subarray_id: Optional[int] {static}
  band_5_tuning: list[float] {static}
}
class ska_oso_pdm.sb_definition.csp.lowcbf.LowCBFConfiguration {
  stations: StationConfiguration {static}
  vis: VisConfiguration {static}
}
class ska_oso_pdm.sb_definition.csp.pss.PSSConfiguration {
}
class ska_oso_pdm.sb_definition.csp.pst.PSTConfiguration {
}
class ska_oso_pdm.sb_definition.csp.subarray.SubarrayConfiguration {
  subarray_name: str {static}
}
class ska_oso_pdm.sb_definition.csp.lowcbf.StationBeamConfiguration {
  stn_beam_id: int {static}
  freq_ids: List[int] {static}
}
class ska_oso_pdm.sb_definition.csp.lowcbf.StationBeamHost {
  sdp_channel: int {static}
  ip_addr: str {static}
}
class ska_oso_pdm.sb_definition.csp.lowcbf.StationBeamMac {
  sdp_channel: int {static}
  server_mac: str {static}
}
class ska_oso_pdm.sb_definition.csp.lowcbf.StationBeamPort {
  sdp_channel: int {static}
  udp_port: int {static}
  stride: int {static}
}
class ska_oso_pdm.sb_definition.csp.lowcbf.StationConfiguration {
  stns: List[List[int]] {static}
  stn_beams: List[StationBeamConfiguration] {static}
}
class ska_oso_pdm.sb_definition.csp.lowcbf.VisConfiguration {
  fsp: VisFSPConfiguration {static}
  stn_beams: List[VisStationBeamConfiguration] {static}
}
class ska_oso_pdm.sb_definition.csp.lowcbf.VisFSPConfiguration {
  function_mode: str {static}
  fsp_ids: List[int] {static}
}
class ska_oso_pdm.sb_definition.csp.lowcbf.VisStationBeamConfiguration {
  stn_beam_id: int {static}
  integration_ms: timedelta {static}
  host: List[StationBeamHost] {static}
  port: List[StationBeamPort] {static}
  mac: Optional[List[StationBeamMac]] {static}
}
enum ska_oso_pdm.sb_definition.dish.dish_configuration.ReceiverBand {
  BAND_1: 1 {static}
  BAND_2: 2 {static}
  BAND_5A: 5a {static}
  BAND_5B: 5b {static}
}
class ska_oso_pdm.sb_definition.procedures.FilesystemScript {
  kind: Literal[ScriptKind] {static}
  path: str {static}
}
class ska_oso_pdm.sb_definition.procedures.GitScript {
  kind: Literal[ScriptKind] {static}
  repo: str {static}
  path: str {static}
  branch: Optional[str] {static}
  commit: Optional[str] {static}
}
class ska_oso_pdm.sb_definition.procedures.InlineScript {
  kind: Literal[ScriptKind] {static}
  content: str {static}
}
abstract class ska_oso_pdm.sb_definition.procedures.PythonProcedure {
  function_args: dict[str, PythonArguments] {static}
}
enum ska_oso_pdm.sb_definition.procedures.ScriptKind {
  INLINE: inline {static}
  FILESYSTEM: filesystem {static}
  GIT: git {static}
}
class ska_oso_pdm.sb_definition.sdp.beam.Beam {
  beam_id: str {static}
  function: BeamFunction {static}
  search_beam_id: Optional[int] {static}
  timing_beam_id: Optional[int] {static}
  vlbi_beam_id: Optional[int] {static}
}
enum ska_oso_pdm.sb_definition.sdp.beam.BeamFunction {
  VISIBILITIES: visibilities {static}
  PULSAR_SEARCH: pulsar search {static}
  PULSAR_TIMING: pulsar timing {static}
  VLBI: vlbi {static}
  TRANSIENT_BUFFER: transient buffer {static}
}
class ska_oso_pdm.sb_definition.sdp.channels.Channels {
  channels_id: str {static}
  spectral_windows: list[SpectralWindow] {static}
}
class ska_oso_pdm.sb_definition.sdp.channels.SpectralWindow {
  spectral_window_id: str {static}
  count: int {static}
  start: int {static}
  freq_min: float {static}
  freq_max: float {static}
  stride: Optional[int] {static}
  link_map: list[tuple] {static}
}
class ska_oso_pdm.sb_definition.sdp.execution_block.ExecutionBlock {
  eb_id: str {static}
  max_length: float {static}
  context: dict {static}
  beams: list[Beam] {static}
  scan_types: list[ScanType] {static}
  channels: list[Channels] {static}
  polarisations: list[Polarisation] {static}
}
class ska_oso_pdm.sb_definition.sdp.polarisation.Polarisation {
  polarisations_id: str {static}
  corr_type: list[str] {static}
}
class ska_oso_pdm.sb_definition.sdp.scan_type.ScanType {
  scan_type_id: str {static}
  derive_from: Optional[str] {static}
  beams: list[BeamMapping] {static}
}
class ska_oso_pdm.sb_definition.sdp.processing_block.PbDependency {
  pb_ref: str {static}
  kind: list[str] {static}
}
class ska_oso_pdm.sb_definition.sdp.processing_block.ProcessingBlock {
  pb_id: str {static}
  script: Script {static}
  sbi_refs: list[str] {static}
  parameters: dict {static}
  dependencies: list[PbDependency] {static}
}
class ska_oso_pdm.sb_definition.sdp.processing_block.Script {
  name: str {static}
  kind: ScriptKind {static}
  version: str {static}
}
enum ska_oso_pdm.sb_definition.sdp.processing_block.ScriptKind {
  REALTIME: realtime {static}
  BATCH: batch {static}
}
class ska_oso_pdm.sb_definition.sdp.resources.Resources {
  csp_links: list[int] {static}
  receptors: list[str] {static}
  receive_nodes: Optional[int] {static}
}
class ska_oso_pdm.sb_definition.sdp.scan_type.BeamMapping {
  beam_ref: str {static}
  field_ref: Optional[str] {static}
  channels_ref: Optional[str] {static}
  polarisations_ref: Optional[str] {static}
}
ska_oso_pdm.sb_definition.csp.csp_configuration.CSPConfiguration *-- ska_oso_pdm.sb_definition.csp.subarray.SubarrayConfiguration
ska_oso_pdm.sb_definition.csp.csp_configuration.CSPConfiguration *-- ska_oso_pdm.sb_definition.csp.common.CommonConfiguration
ska_oso_pdm.sb_definition.csp.csp_configuration.CSPConfiguration *-- ska_oso_pdm.sb_definition.csp.cbf.CBFConfiguration
ska_oso_pdm.sb_definition.csp.csp_configuration.CSPConfiguration *-- ska_oso_pdm.sb_definition.csp.lowcbf.LowCBFConfiguration
ska_oso_pdm.sb_definition.csp.csp_configuration.CSPConfiguration *-- ska_oso_pdm.sb_definition.csp.pst.PSTConfiguration
ska_oso_pdm.sb_definition.csp.csp_configuration.CSPConfiguration *-- ska_oso_pdm.sb_definition.csp.pss.PSSConfiguration
ska_oso_pdm.sb_definition.dish.dish_configuration.DishConfiguration *-- ska_oso_pdm.sb_definition.dish.dish_configuration.ReceiverBand
ska_oso_pdm.sb_definition.sb_definition.SBDefinition *-- ska_oso_pdm.sb_definition.procedures.InlineScript
ska_oso_pdm.sb_definition.sb_definition.SBDefinition *-- ska_oso_pdm.sb_definition.scan_definition.ScanDefinition
ska_oso_pdm.sb_definition.sb_definition.SBDefinition *-- ska_oso_pdm.sb_definition.sdp.sdp_configuration.SDPConfiguration
ska_oso_pdm.sb_definition.sb_definition.SBDefinition *-- ska_oso_pdm.sb_definition.dish.dish_configuration.DishConfiguration
ska_oso_pdm.sb_definition.sb_definition.SBDefinition *-- ska_oso_pdm.sb_definition.csp.csp_configuration.CSPConfiguration
ska_oso_pdm.sb_definition.sb_definition.SBDefinition *-- ska_oso_pdm.sb_definition.dish.dish_allocation.DishAllocation
ska_oso_pdm.sb_definition.sb_definition.SBDefinition *-- ska_oso_pdm.sb_definition.mccs.mccs_allocation.MCCSAllocation
ska_oso_pdm.sb_definition.sdp.sdp_configuration.SDPConfiguration *-- ska_oso_pdm.sb_definition.sdp.execution_block.ExecutionBlock
ska_oso_pdm.sb_definition.sdp.sdp_configuration.SDPConfiguration *-- ska_oso_pdm.sb_definition.sdp.resources.Resources
ska_oso_pdm.sb_definition.sdp.sdp_configuration.SDPConfiguration *-- ska_oso_pdm.sb_definition.sdp.processing_block.ProcessingBlock
ska_oso_pdm.sb_definition.scan_definition.ScanDefinition *-- ska_oso_pdm.sb_definition.scan_definition.PointingCorrection
ska_oso_pdm.sb_definition.csp.cbf.CBFConfiguration *-- ska_oso_pdm.sb_definition.csp.cbf.FSPConfiguration
ska_oso_pdm.sb_definition.csp.cbf.CBFConfiguration *-- ska_oso_pdm.sb_definition.csp.cbf.VLBIConfiguration
ska_oso_pdm.sb_definition.csp.cbf.FSPConfiguration *-- ska_oso_pdm.sb_definition.csp.cbf.FSPFunctionMode
ska_oso_pdm.sb_definition.csp.lowcbf.LowCBFConfiguration *-- ska_oso_pdm.sb_definition.csp.lowcbf.StationConfiguration
ska_oso_pdm.sb_definition.csp.lowcbf.LowCBFConfiguration *-- ska_oso_pdm.sb_definition.csp.lowcbf.VisConfiguration
ska_oso_pdm.sb_definition.csp.lowcbf.StationConfiguration *-- ska_oso_pdm.sb_definition.csp.lowcbf.StationBeamConfiguration
ska_oso_pdm.sb_definition.csp.lowcbf.VisConfiguration *-- ska_oso_pdm.sb_definition.csp.lowcbf.VisFSPConfiguration
ska_oso_pdm.sb_definition.csp.lowcbf.VisConfiguration *-- ska_oso_pdm.sb_definition.csp.lowcbf.VisStationBeamConfiguration
ska_oso_pdm.sb_definition.csp.lowcbf.VisStationBeamConfiguration *-- ska_oso_pdm.sb_definition.csp.lowcbf.StationBeamHost
ska_oso_pdm.sb_definition.csp.lowcbf.VisStationBeamConfiguration *-- ska_oso_pdm.sb_definition.csp.lowcbf.StationBeamPort
ska_oso_pdm.sb_definition.csp.lowcbf.VisStationBeamConfiguration *-- ska_oso_pdm.sb_definition.csp.lowcbf.StationBeamMac
ska_oso_pdm.sb_definition.procedures.FilesystemScript *-- ska_oso_pdm.sb_definition.procedures.ScriptKind
ska_oso_pdm.sb_definition.procedures.PythonProcedure <|-- ska_oso_pdm.sb_definition.procedures.FilesystemScript
ska_oso_pdm.sb_definition.procedures.GitScript *-- ska_oso_pdm.sb_definition.procedures.ScriptKind
ska_oso_pdm.sb_definition.procedures.FilesystemScript <|-- ska_oso_pdm.sb_definition.procedures.GitScript
ska_oso_pdm.sb_definition.procedures.InlineScript *-- ska_oso_pdm.sb_definition.procedures.ScriptKind
ska_oso_pdm.sb_definition.procedures.PythonProcedure <|-- ska_oso_pdm.sb_definition.procedures.InlineScript
ska_oso_pdm.sb_definition.sdp.beam.Beam *-- ska_oso_pdm.sb_definition.sdp.beam.BeamFunction
ska_oso_pdm.sb_definition.sdp.channels.Channels *-- ska_oso_pdm.sb_definition.sdp.channels.SpectralWindow
ska_oso_pdm.sb_definition.sdp.execution_block.ExecutionBlock *-- ska_oso_pdm.sb_definition.sdp.beam.Beam
ska_oso_pdm.sb_definition.sdp.execution_block.ExecutionBlock *-- ska_oso_pdm.sb_definition.sdp.scan_type.ScanType
ska_oso_pdm.sb_definition.sdp.execution_block.ExecutionBlock *-- ska_oso_pdm.sb_definition.sdp.channels.Channels
ska_oso_pdm.sb_definition.sdp.execution_block.ExecutionBlock *-- ska_oso_pdm.sb_definition.sdp.polarisation.Polarisation
ska_oso_pdm.sb_definition.sdp.scan_type.ScanType *-- ska_oso_pdm.sb_definition.sdp.scan_type.BeamMapping
ska_oso_pdm.sb_definition.sdp.processing_block.ProcessingBlock *-- ska_oso_pdm.sb_definition.sdp.processing_block.Script
ska_oso_pdm.sb_definition.sdp.processing_block.ProcessingBlock *-- ska_oso_pdm.sb_definition.sdp.processing_block.PbDependency
ska_oso_pdm.sb_definition.sdp.processing_block.Script *-- ska_oso_pdm.sb_definition.sdp.processing_block.ScriptKind

@enduml


===========
ska-oso-pdm
===========

.. HOME SECTION ==================================================

.. Hidden toctree to manage the sidebar navigation.

.. toctree::
  :maxdepth: 1
  :caption: Changelog
  :hidden:

  CHANGELOG.rst

.. toctree::
  :maxdepth: 3
  :caption: Table of Contents
  :hidden:

  background
  layout/layout
  builders
  json

.. COMMUNITY SECTION ==================================================

.. Hidden toctree to manage the sidebar navigation.

.. toctree::
  :maxdepth: 1
  :caption: API Documentation
  :hidden:

  api/entity_status_history
  api/execution_block
  api/project
  api/proposal
  api/sb_definition/sb_definition
  api/sb_definition/scan_definition
  api/sb_definition/procedures
  api/sb_definition/csp/csp
  api/sb_definition/dish/dish
  api/sb_definition/mccs/mccs
  api/sb_definition/sdp/sdp
  api/sb_instance
  api/_shared/__init__
  api/_shared/target
  api/builders

Project description
===================

The SKA Project Data Model (PDM) is the data model used for 'offline'
observatory processes. PDM entities such as observing proposals, observing
programmes, scheduling blocks, etc., capture all the information required to
describe and grade an observing proposal and any associated scheduling blocks.
ska-oso-pdm is a Python object model and JSON serialisation library for the
PDM.

Status
------
The data models evolve from PI to PI as more elements are added to the
system and greater element configuration space is exposed. Practically
speaking, this library should be considered the best reference for the current
state of the design for the SKA project data models.

For examples of SBDefinitions, see the :ref:`MID JSON` and :ref:`LOW JSON`.

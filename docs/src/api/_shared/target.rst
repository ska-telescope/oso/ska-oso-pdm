.. _api-_shared_target:

**************************
ska_oso_pdm._shared.target
**************************

The ska_oso_pdm._shared.target module models entities concerned with
receptor pointing and mapping (source coordinates, survey fields,
pointing patterns, etc.). The contents of the module are presented
in the diagram below.

.. figure:: ../../uml/ska_oso_pdm__shared_target_coordinates.svg
   :align: center

   Classes specifically associated with target coordinates


.. figure:: ../../uml/ska_oso_pdm__shared_target.svg
   :align: center

   Classes associated with targets and pointing patterns


An example serialisation of this model to JSON is shown below. This
describes two target objects: one, a five-point observation centred
on Polaris Australis using 5 arcsec offsets between individual pointings,
and two, a single pointing observation centred on M83. In both cases,
the coordinates are defined using equatorial coordinates using an ICRS
reference frame.

.. literalinclude:: ../../../../tests/unit/ska_oso_pdm/serialisation_cases/sb_definition/testfile_sample_mid_sb.json
    :language: JSON
    :start-at: "targets": [
    :end-before: "sdp_configuration": {

Another JSON example defining two drift scan targets with position specified
as azimuth and elevation is shown below.

.. literalinclude:: ../../../../tests/unit/ska_oso_pdm/serialisation_cases/sb_definition/testfile_sample_low_sb.json
    :language: JSON
    :start-at: "targets": [
    :end-before: "mccs_allocation": {

.. automodule:: ska_oso_pdm._shared.target
    :members:
    :exclude-members: model_computed_fields,model_config,model_fields,model_post_init,handle_units

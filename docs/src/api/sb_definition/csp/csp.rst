.. _api-sb_definition_csp:

*****************************
ska_oso_pdm.sb_definition.csp
*****************************

The csp package models SB entities concerned with CSP (Central
Signal Processing) beam former and correlator configuration. Package
contents are presented in the diagram below.

.. figure:: ../../../uml/ska_oso_pdm_sb_definition_csp.svg
   :align: center

   Class diagram for the modules within csp

An example serialisation of SKA-Mid CSP to JSON is shown below.

.. literalinclude:: ../../../../../tests/unit/ska_oso_pdm/serialisation_cases/sb_definition/testfile_sample_mid_sb.json
    :language: JSON
    :start-at: "csp_configurations": [
    :end-before: "dish_allocations": {

An example serialisation of SKA-Low CSP to JSON is shown below.

.. literalinclude:: ../../../../../tests/unit/ska_oso_pdm/serialisation_cases/sb_definition/testfile_sample_low_sb.json
    :language: JSON
    :start-at: "csp_configurations": [

.. automodule:: ska_oso_pdm.sb_definition.csp.csp_configuration
    :members:
    :exclude-members: model_computed_fields,model_config,model_fields,model_post_init,handle_units

.. automodule:: ska_oso_pdm.sb_definition.csp.common
    :members:
    :exclude-members: model_computed_fields,model_config,model_fields,model_post_init,handle_units

.. automodule:: ska_oso_pdm.sb_definition.csp.cbf
    :members:
    :exclude-members: model_computed_fields,model_config,model_fields,model_post_init,handle_units

.. automodule:: ska_oso_pdm.sb_definition.csp.midcbf
    :members:
    :exclude-members: model_computed_fields,model_config,model_fields,model_post_init,handle_units

.. automodule:: ska_oso_pdm.sb_definition.csp.lowcbf
    :members:
    :exclude-members: model_computed_fields,model_config,model_fields,model_post_init,handle_units

.. automodule:: ska_oso_pdm.sb_definition.csp.subarray
    :members:
    :exclude-members: model_computed_fields,model_config,model_fields,model_post_init,handle_units

.. automodule:: ska_oso_pdm.sb_definition.csp.pst
    :members:
    :exclude-members: model_computed_fields,model_config,model_fields,model_post_init,handle_units

.. automodule:: ska_oso_pdm.sb_definition.csp.pss
    :members:
    :exclude-members: model_computed_fields,model_config,model_fields,model_post_init,handle_units

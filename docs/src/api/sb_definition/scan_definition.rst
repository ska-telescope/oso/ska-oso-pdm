.. _api-sb_definition_scan_definition:

*****************************************
ska_oso_pdm.sb_definition.scan_definition
*****************************************

The ska_oso_pdm.sb_definition.scan_definition module models SB entities
concerned with the selection of which element configurations should take
effect for the duration of a scan. The contents of the module are presented in
the diagram below.

.. figure:: ../../uml/ska_oso_pdm_sb_definition_scan_definition.svg
   :align: center

   Class diagram for the scan_definition module

An example serialisation of this model to JSON for SKA MID is shown below.

.. literalinclude:: ../../../../tests/unit/ska_oso_pdm/serialisation_cases/sb_definition/testfile_sample_mid_sb.json
    :start-at: "scan_definitions": [
    :end-before: "scan_sequence": [

A JSON code example for SKA LOW is shown below.

.. literalinclude:: ../../../../tests/unit/ska_oso_pdm/serialisation_cases/sb_definition/testfile_sample_low_sb.json
    :start-at: "scan_definitions": [
    :end-before: "scan_sequence": [

.. automodule:: ska_oso_pdm.sb_definition.scan_definition
    :members:
    :exclude-members: model_computed_fields,model_config,model_fields,model_post_init,handle_units

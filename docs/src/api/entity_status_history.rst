.. _api-entity_status_history:

*********************************
ska_oso_pdm.entity_status_history
*********************************

.. figure:: ../uml/ska_oso_pdm_entity_status_history.svg
   :align: center

   Class diagram for an Entity Status History


.. automodule:: ska_oso_pdm.entity_status_history
    :members:
    :exclude-members: model_computed_fields,model_config,model_fields,model_post_init,handle_units

import pytest

from ska_oso_pdm.builders import MidSBDefinitionBuilder, add_scan_for_each_target
from ska_oso_pdm.builders.target_builder import MidTargetBuilder
from ska_oso_pdm.builders.utils import DEFAULT_SCAN_DURATION


@pytest.mark.parametrize(
    "scan_durations, expected_scan_durations",
    (
        (None, [DEFAULT_SCAN_DURATION, DEFAULT_SCAN_DURATION]),
        (500.0, [500.0, 500.0]),
        ([400.0, 500.0], [400.0, 500.0]),
    ),
)
def test_add_scan_for_each_target_single(scan_durations, expected_scan_durations):
    targets = [
        MidTargetBuilder(target_id="test target 1"),
        MidTargetBuilder(target_id="test target 2"),
    ]
    sb = MidSBDefinitionBuilder(targets=targets)
    sb_with_scan_definitions = add_scan_for_each_target(sb, scan_durations)

    assert len(sb_with_scan_definitions.scan_definitions) == 2
    assert all(
        scan_def.target_ref in ["test target 1", "test target 2"]
        for scan_def in sb_with_scan_definitions.scan_definitions
    )
    assert [
        scan_def.scan_duration_ms.total_seconds() * 1000
        for scan_def in sb_with_scan_definitions.scan_definitions
    ] == expected_scan_durations

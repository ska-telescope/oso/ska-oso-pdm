"""
Unit tests for the ska_oso_pdm._shared.target module.
"""

import copy

import pytest
from astropy.coordinates import SkyCoord
from astropy.units import Quantity
from pydantic import ValidationError

from ska_oso_pdm._shared.target import (
    CrossScanParameters,
    EquatorialCoordinates,
    EquatorialCoordinatesReferenceFrame,
    FivePointParameters,
    GalacticCoordinates,
    HorizontalCoordinates,
    PointingKind,
    PointingPattern,
    RadialVelocity,
    RasterParameters,
    SinglePointParameters,
    SolarSystemObject,
    SolarSystemObjectName,
    SpiralParameters,
    StarRasterParameters,
    Target,
)

COORD = SkyCoord(ra=1, dec=0.5, frame="icrs", unit=("hourangle", "deg"))


class TestBaseCoordinates:

    @pytest.mark.parametrize(
        "coordinate",
        [
            EquatorialCoordinates(ra=1, dec=0.5),
            HorizontalCoordinates(az=1, el=0.5),
            GalacticCoordinates(l=1, b=0.5),
            SolarSystemObject(name=SolarSystemObjectName.MARS),
        ],
    )
    def test_to_sky_coord(self, coordinate):
        # Asserts that a SkyCoord is correctly created from the Coordinate and astropy does not throw any errors
        assert coordinate.to_sky_coord()


class TestEquatorialCoordinates:
    def test_defaults(self):
        """
        Verify EquatorialCoordinates default arguments.
        """
        target_1 = EquatorialCoordinates(ra=1, dec=0.5)
        target_2 = EquatorialCoordinates(
            ra=1,
            dec=0.5,
            reference_frame=EquatorialCoordinatesReferenceFrame.ICRS,
            unit=("hourangle", "deg"),
        )
        assert target_1 == target_2

    def test_eq(self):
        """
        Verify that EquatorialCoordinates objects are considered equal when:

          - they point to the same place on the sky
          - they use the same co-ordinate frame
          - they use the same co-ordinate units
        """
        target_1 = EquatorialCoordinates(
            ra=1,
            dec=2,
            reference_frame=EquatorialCoordinatesReferenceFrame.FK5,
            unit=("deg", "deg"),
        )
        target_2 = EquatorialCoordinates(
            ra=1,
            dec=2,
            reference_frame=EquatorialCoordinatesReferenceFrame.FK5,
            unit=("deg", "deg"),
        )
        target_3 = EquatorialCoordinates(ra=1, dec=1)
        assert target_1 == target_2
        assert target_1 != target_3

    def test_not_equal_to_other_objects(self):
        """
        Verify that EquatorialCoordinates objects are considered unequal to other objects.
        """
        target = EquatorialCoordinates(
            ra=1,
            dec=2,
            reference_frame=EquatorialCoordinatesReferenceFrame.FK5,
            unit=("deg", "deg"),
        )
        assert target != object

    def test_repr(self):
        """
        Verify the repr representation of a Target.
        """
        target = EquatorialCoordinates(
            ra=30,
            dec=-3600,
            reference_frame=EquatorialCoordinatesReferenceFrame.ICRS,
            unit=("deg", "arcsec"),
        )
        expected = "EquatorialCoordinates(kind=CoordinateKind.EQUATORIAL, ra=30.0, dec=-3600.0, reference_frame=EquatorialCoordinatesReferenceFrame.ICRS, epoch=2000.0, unit=('deg', 'arcsec'))"
        assert expected == repr(target)

    def test_str(self):
        """
        Verify the string representation of a EquatorialCoordinates.
        """
        target = EquatorialCoordinates(
            ra=30,
            dec="0",
            reference_frame=EquatorialCoordinatesReferenceFrame.ICRS,
            unit=("deg", "rad"),
        )
        expected = (
            "kind=CoordinateKind.EQUATORIAL ra=30.0 dec='0' "
            "reference_frame=EquatorialCoordinatesReferenceFrame.ICRS epoch=2000.0 "
            "unit=('deg', 'rad')"
        )
        assert expected == str(target)


class TestHorizontalCoordinates:
    def test_defaults(self):
        """
        Verify HorizontalCoordinates default arguments.
        """
        target_1 = HorizontalCoordinates(az=1, el=0.5)
        target_2 = HorizontalCoordinates(az=1, el=0.5, unit=("deg", "deg"))
        assert target_1 == target_2

    def test_eq(self):
        """
        Verify that HorizontalCoordinates objects are considered equal when:

          - they point to the same place on the sky
        """
        target_1 = HorizontalCoordinates(az=1, el=2)
        target_2 = HorizontalCoordinates(az=1, el=2)
        target_3 = HorizontalCoordinates(az=1, el=1)
        assert target_1 == target_2
        assert target_1 != target_3

    def test_not_equal_to_other_objects(self):
        """
        Verify that HorizontalCoordinates objects are considered unequal to other objects.
        """
        target = HorizontalCoordinates(az=1, el=2)
        assert target != object

    def test_repr(self):
        """
        Verify the repr representation of a HorizontalCoordinates.
        """
        target = HorizontalCoordinates(az=180.0, el=45.0, unit=("deg", "deg"))
        expected = (
            "HorizontalCoordinates(kind=CoordinateKind.HORIZONTAL, az=180.0, "
            "el=45.0, unit=('deg', 'deg'), reference_frame=HorizontalCoordinatesReferenceFrame.ALTAZ)"
        )
        assert expected == repr(target)

    def test_str(self):
        """
        Verify the string representation of a HorizontalCoordinates.
        """
        target = HorizontalCoordinates(az=180.0, el=45.0, unit=("deg", "deg"))
        expected = (
            "kind=CoordinateKind.HORIZONTAL az=180.0 el=45.0 unit=('deg', 'deg') "
            "reference_frame=HorizontalCoordinatesReferenceFrame.ALTAZ"
        )
        assert expected == str(target)


class TestGalacticCoordinates:
    def test_defaults(self):
        """
        Verify GalacticCoordinates default arguments.
        """
        target_1 = GalacticCoordinates(l=1, b=0.5)
        target_2 = GalacticCoordinates(l=1, b=0.5, unit=("deg", "deg"))
        assert target_1 == target_2

    def test_eq(self):
        """
        Verify that GalacticCoordinates objects are considered equal when:

          - they point to the same place on the sky
        """
        target_1 = GalacticCoordinates(l=1, b=2)
        target_2 = GalacticCoordinates(l=1, b=2)
        target_3 = GalacticCoordinates(l=1, b=1)
        assert target_1 == target_2
        assert target_1 != target_3

    def test_not_equal_to_other_objects(self):
        """
        Verify that GalacticCoordinates objects are considered unequal to other objects.
        """
        target = GalacticCoordinates(l=1, b=2)
        assert target != object

    def test_repr(self):
        """
        Verify the repr representation of a GalacticCoordinates.
        """
        target = GalacticCoordinates(l=180.0, b=45.0, unit=("deg", "deg"))
        expected = (
            "GalacticCoordinates(kind=CoordinateKind.GALACTIC, l=180.0, "
            "b=45.0, pm_l=0.0, pm_b=0.0, epoch=2000.0, parallax=0.0)"
        )
        assert expected == repr(target)

    def test_str(self):
        """
        Verify the string representation of a GalacticCoordinates.
        """
        target = GalacticCoordinates(l=180.0, b=45.0, unit=("deg", "deg"))
        expected = (
            "kind=CoordinateKind.GALACTIC l=180.0 b=45.0 "
            "pm_l=0.0 pm_b=0.0 epoch=2000.0 parallax=0.0"
        )
        assert expected == str(target)


class TestSolarSystemObject:
    def test_eq(self):
        """
        Verify that SolarSystemObject instances are considered equal when:

          - they point to the same planet
        """
        target_1 = SolarSystemObject(name=SolarSystemObjectName.MARS)
        target_2 = SolarSystemObject(name=SolarSystemObjectName.MARS)
        target_3 = SolarSystemObject(name=SolarSystemObjectName.VENUS)
        target_4 = SolarSystemObject(name=SolarSystemObjectName.SUN)
        target_5 = SolarSystemObject(name=SolarSystemObjectName.URANUS)
        target_6 = SolarSystemObject(name=SolarSystemObjectName.URANUS)
        target_7 = SolarSystemObject(name=SolarSystemObjectName.NEPTUNE)
        assert target_1 == target_2
        assert target_5 == target_6
        assert target_1 != target_3
        assert target_4 != target_7

    def test_is_not_equal_to_other_objects(self):
        """
        Verify that EquatorialCoordinates objects are considered unequal to other objects.
        """
        planet = SolarSystemObject(name=SolarSystemObjectName.MARS)
        assert planet != EquatorialCoordinates(ra=0, dec=0)
        assert planet != object

    def test_repr(self):
        """
        Verify the repr representation of a SolarSystemObject.
        """
        mars = SolarSystemObject(name=SolarSystemObjectName.MARS)
        expected = "SolarSystemObject(kind=CoordinateKind.SSO, reference_frame='special', name=SolarSystemObjectName.MARS)"
        assert repr(mars) == expected

    def test_str(self):
        """
        Verify the string representation of a SolarSystemObject.
        """
        mars = SolarSystemObject(name=SolarSystemObjectName.MARS)
        expected = (
            "kind=CoordinateKind.SSO reference_frame='special' "
            "name=SolarSystemObjectName.MARS"
        )
        assert str(mars) == expected


class TestCrossScanParameters:
    def test_defaults(self):
        x = CrossScanParameters()
        y = CrossScanParameters(offset_arcsec=0.0)
        assert x == y

    def test_eq(self):
        x = CrossScanParameters(offset_arcsec=1.0)
        y = CrossScanParameters(offset_arcsec=1.0)
        z = CrossScanParameters(offset_arcsec=0.0)
        assert x == y
        assert y == x
        assert z != x

    def test_not_eq_to_other_objects(self):
        x = CrossScanParameters(offset_arcsec=1.0)
        y = FivePointParameters(offset_arcsec=1.0)
        assert x != y

    def test_repr(self):
        x = CrossScanParameters(offset_arcsec=1.0)
        assert (
            repr(x)
            == "CrossScanParameters(kind=PointingKind.CROSS_SCAN, offset_arcsec=1.0)"
        )


class TestFivePointParameters:
    def test_defaults(self):
        x = FivePointParameters()
        y = FivePointParameters(offset_arcsec=0.0)
        assert x == y

    def test_eq(self):
        x = FivePointParameters(offset_arcsec=1.0)
        y = FivePointParameters(offset_arcsec=1.0)
        z = FivePointParameters(offset_arcsec=0.0)
        assert x == y
        assert y == x
        assert z != x

    def test_not_eq_to_other_objects(self):
        x = FivePointParameters(offset_arcsec=1.0)
        y = CrossScanParameters(offset_arcsec=1.0)
        assert x != y

    def test_repr(self):
        x = FivePointParameters(offset_arcsec=1.0)
        assert (
            repr(x)
            == "FivePointParameters(kind=PointingKind.FIVE_POINT, offset_arcsec=1.0)"
        )


class TestSinglePointParameters:
    def test_defaults(self):
        x = SinglePointParameters()
        y = SinglePointParameters(offset_x_arcsec=0.0, offset_y_arcsec=0.0)
        assert x == y

    def test_eq(self):
        x = SinglePointParameters(offset_x_arcsec=1.0, offset_y_arcsec=1.0)
        assert x == SinglePointParameters(offset_x_arcsec=1.0, offset_y_arcsec=1.0)
        assert x != SinglePointParameters(offset_x_arcsec=1.0, offset_y_arcsec=0.0)
        assert x != SinglePointParameters(offset_x_arcsec=0.0, offset_y_arcsec=1.0)

    def test_not_eq_to_other_objects(self):
        x = SinglePointParameters()
        assert x != CrossScanParameters()
        assert x != object

    def test_repr(self):
        x = SinglePointParameters()
        assert (
            repr(x)
            == "SinglePointParameters(kind=PointingKind.SINGLE_POINT, offset_x_arcsec=0.0, offset_y_arcsec=0.0)"
        )


class TestRasterParameters:
    def test_defaults(self):
        x = RasterParameters()
        y = RasterParameters(
            row_length_arcsec=0.0,
            row_offset_arcsec=0.0,
            n_rows=1,
            pa=0.0,
            unidirectional=False,
        )
        assert x == y

    def test_eq(self):
        x = RasterParameters()
        y = RasterParameters()
        assert x == y

        non_defaults = dict(
            row_length_arcsec=1.0,
            row_offset_arcsec=1.0,
            n_rows=2,
            pa=1.0,
            unidirectional=True,
        )
        for k, v in non_defaults.items():
            y = RasterParameters()
            setattr(y, k, v)
            assert y != x

    def test_not_eq_to_other_objects(self):
        x = RasterParameters()
        y = CrossScanParameters()
        assert x != y

    def test_repr(self):
        x = RasterParameters()
        assert (
            repr(x)
            == "RasterParameters(kind=PointingKind.RASTER, row_length_arcsec=0.0, row_offset_arcsec=0.0, n_rows=1, pa=0.0, unidirectional=False)"
        )


class TestStarRasterParameters:
    def test_defaults(self):
        x = StarRasterParameters()
        y = StarRasterParameters(
            row_length_arcsec=0.0, n_rows=1, row_offset_angle=0.0, unidirectional=False
        )
        assert x == y

    def test_eq(self):
        x = StarRasterParameters()
        y = StarRasterParameters()
        assert x == y

        non_defaults = dict(
            row_length_arcsec=1.0,
            n_rows=2,
            row_offset_angle=1.0,
            unidirectional=True,
        )
        for k, v in non_defaults.items():
            y = StarRasterParameters()
            setattr(y, k, v)
            assert y != x

    def test_not_eq_to_other_objects(self):
        x = StarRasterParameters()
        y = RasterParameters()
        assert x != y

    def test_repr(self):
        x = StarRasterParameters()
        assert (
            repr(x)
            == "StarRasterParameters(kind=PointingKind.STAR_RASTER, row_length_arcsec=0.0, n_rows=1, row_offset_angle=0.0, unidirectional=False)"
        )


class TestSpiralParameters:
    def test_defaults(self):
        x = SpiralParameters()
        y = SpiralParameters(
            scan_extent=Quantity(value=10.0, unit="deg"),
            track_time=Quantity(value=10.0, unit="s"),
            cycle_track_time=Quantity(value=30.0, unit="s"),
            slow_time=Quantity(value=6.0, unit="s"),
            sample_time=Quantity(value=0.25, unit="s"),
        )
        assert x == y

    def test_eq(self):
        x = SpiralParameters()
        y = SpiralParameters()
        assert x == y

        non_defaults = dict(
            scan_extent=Quantity(value=11.0, unit="deg"),
            track_time=Quantity(value=12.0, unit="s"),
            cycle_track_time=Quantity(value=20.0, unit="s"),
            slow_time=Quantity(value=6.5, unit="s"),
            sample_time=Quantity(value=2.25, unit="s"),
            scan_speed=Quantity(value=2.1, unit="deg / s"),
            slew_speed=Quantity(value=-1.4, unit="deg / s"),
            twist_factor=0.5,
            high_el_slowdown_factor=2.1,
        )
        for k, v in non_defaults.items():
            y = SpiralParameters()
            setattr(y, k, v)
            assert y != x

    def test_not_eq_to_other_objects(self):
        x = SpiralParameters()
        y = SpiralParameters(
            scan_extent=Quantity(value=11.0, unit="deg"),
            track_time=Quantity(value=12.0, unit="s"),
            cycle_track_time=Quantity(value=20.0, unit="s"),
            slow_time=Quantity(value=6.5, unit="s"),
            sample_time=Quantity(value=2.25, unit="s"),
            scan_speed=Quantity(value=2.1, unit="deg / s"),
            slew_speed=Quantity(value=-1.4, unit="deg / s"),
            twist_factor=0.25,
            high_el_slowdown_factor=2.1,
        )
        assert x != y


class TestPointingPattern:
    def test_defaults(self):
        x = PointingPattern()
        y = PointingPattern(
            active=PointingKind.SINGLE_POINT, parameters=[SinglePointParameters()]
        )

        assert x == y

    def test_active_parameters_must_be_contained(self):
        with pytest.raises(ValueError):
            PointingPattern(active=PointingKind.CROSS_SCAN)
        with pytest.raises(ValueError):
            PointingPattern(
                active=PointingKind.CROSS_SCAN, parameters=[SinglePointParameters()]
            )

    def test_duplicate_parameters(self):
        with pytest.raises(ValueError):
            PointingPattern(
                active=PointingKind.SINGLE_POINT,
                parameters=[SinglePointParameters(), SinglePointParameters()],
            )

    def test_eq(self):
        x = PointingPattern(
            parameters=[SinglePointParameters(), CrossScanParameters()],
            active=PointingKind.CROSS_SCAN,
        )
        other = PointingPattern(
            parameters=[SinglePointParameters(), CrossScanParameters()],
            active=PointingKind.CROSS_SCAN,
        )
        assert x == other

        # changing active should negate equality
        other = PointingPattern(
            parameters=[SinglePointParameters(), CrossScanParameters()],
            active=PointingKind.SINGLE_POINT,
        )
        assert x != other

        # but changing order should not affect equality
        other = PointingPattern(
            parameters=[CrossScanParameters(), SinglePointParameters()],
            active=PointingKind.CROSS_SCAN,
        )
        assert x == other

    def test_repr(self):
        x = PointingPattern()
        expected = (
            "PointingPattern("
            "active=PointingKind.SINGLE_POINT, "
            "parameters=["
            "SinglePointParameters(kind=PointingKind.SINGLE_POINT, offset_x_arcsec=0.0, offset_y_arcsec=0.0)"
            "])"
        )
        assert repr(x) == expected


class TestTarget:
    def test_defaults(self):
        expected = Target(
            target_id="",
            pointing_pattern=PointingPattern(),
            reference_coordinate=EquatorialCoordinates(),
        )
        assert Target() == expected

    def test_eq(self):
        x = Target(
            target_id="target_id",
            pointing_pattern=PointingPattern(
                active=PointingKind.SINGLE_POINT,
                parameters=[
                    SinglePointParameters(offset_x_arcsec=1.0, offset_y_arcsec=2.0)
                ],
            ),
            reference_coordinate=EquatorialCoordinates(ra=1.0, dec=-0.5),
        )

        y = Target(
            target_id="target_id",
            pointing_pattern=PointingPattern(
                active=PointingKind.SINGLE_POINT,
                parameters=[
                    SinglePointParameters(offset_x_arcsec=1.0, offset_y_arcsec=2.0)
                ],
            ),
            reference_coordinate=EquatorialCoordinates(ra=1.0, dec=-0.5),
        )
        assert x == y

        y = copy.deepcopy(x)
        assert x == y

        # changing target ID negates equality
        y.target_id = "foo"
        assert x != y

        y = copy.deepcopy(x)
        y.pointing_pattern = PointingPattern()
        assert x != y

        y = copy.deepcopy(x)
        y.reference_coordinate = EquatorialCoordinates()
        assert x != y

    def test_repr(self):
        target = Target(target_id="foo")
        expected = "<Target=foo | single point on 0 0>"
        assert str(target) == expected


class TestRadialVelocity:
    @pytest.mark.parametrize(
        "quantity",
        [
            dict(value=-12.345, unit="Jy"),
            Quantity(value=-12.345, unit="Jy"),
        ],
    )
    def test_units_are_constrained_on_construction(self, quantity):
        """
        Verify that only radial velocity units are allowed when the
        RadialVelocity instance is constructed
        """
        with pytest.raises(ValidationError):
            RadialVelocity(quantity=quantity)

    def test_units_are_constrained_on_mutation(self):
        """
        Verify that only radial velocity units are allowed when the
        quantity is replaced.

        The Quantity units cannot be directly mutated.
        """
        rv = RadialVelocity()
        with pytest.raises(ValidationError):
            rv.quantity = Quantity(value=-12.345, unit="Jy")

    @pytest.mark.parametrize(
        "constructor_kwargs",
        [dict(redshift=1.23), dict(quantity=Quantity(value=1.23, unit="km / s"))],
    )
    def test_defaults(self, constructor_kwargs):
        """
        Test that a RadialVelocity can be created by specifying redshift or
        quantity, leaving other parameters blank.
        """
        RadialVelocity(**constructor_kwargs)

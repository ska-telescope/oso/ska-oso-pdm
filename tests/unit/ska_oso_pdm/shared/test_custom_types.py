import json
from datetime import timedelta

import pytest
from astropy import units
from pydantic import ValidationError

from ska_oso_pdm._shared import PdmObject, TerseStrEnum, TimedeltaMs
from ska_oso_pdm._shared.custom_types import AstropyQuantity, UnitHelpers


def idfn(val: any):
    return repr(val)


class Obj(PdmObject):
    td: TimedeltaMs


class TestTimedeltaMs:
    """
    Verifies that the custom annotated type, TimedeltaMs behaves as expected.

    - It should interpret numeric input as milliseconds, rather than seconds
    (as normal timedelta)
    - It should serialise to a numeric JSON field in milliseconds.
    - It should accept either numeric input or timedelta() instances
    - It should not modify or convert timedelta input.
    """

    @pytest.mark.parametrize(
        ("in_value", "out_value"),
        (
            ({"td": 3600}, Obj(td=timedelta(seconds=3.6))),
            ({"td": 1000}, Obj(td=timedelta(seconds=1))),
        ),
        ids=idfn,
    )
    def test_load_json(self, in_value: dict[str, any], out_value: Obj):
        result = Obj.model_validate_json(json.dumps(in_value))
        assert result == out_value

    @pytest.mark.parametrize(
        ("in_value", "out_value"),
        (
            ({"td": 3600}, Obj(td=timedelta(seconds=3.6))),
            ({"td": 1000}, Obj(td=timedelta(seconds=1))),
        ),
        ids=idfn,
    )
    def test_load_python(self, in_value: dict[str, any], out_value: Obj):
        result = Obj(**in_value)
        assert result == out_value

    @pytest.mark.parametrize(
        ("in_value", "out_value"),
        (
            (Obj(td=timedelta(seconds=3)), {"td": 3000}),
            (Obj(td=timedelta(milliseconds=25)), {"td": 25}),
            (Obj(td=timedelta(microseconds=100)), {"td": 0.1}),
            (Obj(td=500), {"td": 500}),
        ),
        ids=idfn,
    )
    def test_dump_json(self, in_value: Obj, out_value: dict[str, any]):
        serialised = in_value.model_dump_json()
        result = json.loads(serialised)
        assert result == out_value

    @pytest.mark.parametrize(
        ("in_value", "out_value"),
        (
            (Obj(td=timedelta(seconds=3)), {"td": 3000}),
            (Obj(td=timedelta(milliseconds=25)), {"td": 25}),
            (Obj(td=timedelta(microseconds=100)), {"td": 0.1}),
            (Obj(td=500), {"td": 500}),
        ),
        ids=idfn,
    )
    def test_dump_python(self, in_value: Obj, out_value: dict[str, any]):
        result = in_value.model_dump()
        assert result == out_value

    @pytest.mark.parametrize(
        "in_value",
        (
            "NnA",
            -1000,
            False,
            "100",
            timedelta(0),
            timedelta(days=-5),
        ),
        ids=idfn,
    )
    def test_invalid_inputs(self, in_value: any):
        with pytest.raises(ValidationError):
            Obj(td=in_value)


class QObj(PdmObject):
    q: AstropyQuantity


class TestAstropyQuantity:
    """
    Verifies that the custom annotated type for an AstroPy Quantity behaves as
    expected.
    """

    @pytest.mark.parametrize(
        ("json_dict", "out_value"),
        (
            (
                {"q": {"value": 123.45, "unit": "km/s"}},
                QObj(q=units.Quantity(value=123.45, unit="km/s")),
            ),
            (
                {"q": {"value": 123.45}},
                QObj(q=units.Quantity(value=123.45, unit=units.dimensionless_unscaled)),
            ),
        ),
        ids=idfn,
    )
    def test_load_json(self, json_dict: dict[str, any], out_value: QObj):
        result = QObj.model_validate_json(json.dumps(json_dict))
        assert result == out_value

    @pytest.mark.parametrize(
        ("py_dict", "out_value"),
        (
            (
                {"q": units.Quantity(123.45, "km/s")},
                QObj(q=units.Quantity(123.45, "km/s")),
            ),
            (
                {"q": units.Quantity(value=123.45, unit=units.dimensionless_unscaled)},
                QObj(q=units.Quantity(value=123.45, unit=units.dimensionless_unscaled)),
            ),
            (
                {"q": units.Quantity(value=123.45)},
                QObj(q=units.Quantity(value=123.45, unit=units.dimensionless_unscaled)),
            ),
        ),
        ids=idfn,
    )
    def test_load_python(self, py_dict: dict[str, any], out_value: Obj):
        result = QObj(**py_dict)
        assert result == out_value

    @pytest.mark.parametrize(
        ("instance", "out_value"),
        (
            (
                QObj(q=units.Quantity(123.45, "km/s")),
                {"q": {"value": 123.45, "unit": "km / s"}},
            ),
            (
                QObj(q=units.Quantity(value=123.45, unit=units.dimensionless_unscaled)),
                {"q": {"value": 123.45}},
            ),
            (
                QObj(q=units.Quantity(value=123.45)),
                {"q": {"value": 123.45}},
            ),
        ),
        ids=idfn,
    )
    def test_dump_json(self, instance: QObj, out_value: dict[str, any]):
        serialised = instance.model_dump_json()
        result = json.loads(serialised)
        assert result == out_value

    @pytest.mark.parametrize(
        "bad_input",
        ("NnA", -1000, False, "100", {"value": 123, "unit": "foo"}),
        ids=idfn,
    )
    def test_invalid_inputs(self, bad_input: any):
        with pytest.raises(ValidationError):
            QObj(q=bad_input)


class UnitEnum(TerseStrEnum):
    METERS = "m"
    KILOMETERS = "km"


@pytest.mark.parametrize(
    ("in_value", "valid"),
    (
        ("m", True),
        (UnitEnum.KILOMETERS, True),
        ("foo", False),
        ("second", False),
        (units.Quantity(0, UnitEnum.METERS), True),
        (units.Quantity(0, "second"), False),
    ),
)
def test_constrain_units_to(in_value, valid):
    func = UnitHelpers.constrain_unit_to(UnitEnum)
    if not valid:
        with pytest.raises(ValueError):
            func(in_value)
    else:
        assert func(in_value) == in_value


def test_enum_jsonschema():
    expected = {"type": "string", "enum": ["m", "km"]}
    result = UnitHelpers.enum_jsonschema(UnitEnum)
    assert result == expected

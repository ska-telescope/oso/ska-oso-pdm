from datetime import datetime, timezone
from importlib.metadata import version

import pytest
from pydantic import ValidationError

from ska_oso_pdm._shared.metadata import PDM_VERSION, Metadata

GREATER_VERSION = "10.0.0"
LOWER_VERSION = "0.9.0"


class TestMetadata:
    default_author = "Andy Biggs"
    default_created_on = datetime.now(timezone.utc)
    default_last_modified_on = datetime.now(timezone.utc)

    def test_metadata_without_pdm_version(self):
        """Test the Metadata fields without passing pdm_version"""
        metadata = Metadata(
            created_by=self.default_author,
            version=1,
            created_on=self.default_created_on,
            last_modified_on=self.default_last_modified_on,
            last_modified_by=self.default_author,
        )
        assert metadata.created_by == self.default_author
        assert metadata.last_modified_by == self.default_author
        assert metadata.created_on == self.default_created_on
        assert metadata.last_modified_on == self.default_last_modified_on
        assert metadata.pdm_version == PDM_VERSION

    def test_pdm_version_defaults_to_current(self):
        expected = version("ska-oso-pdm")
        md = Metadata()
        assert md.pdm_version == expected

    def test_metadata_regex_accepts_rc_versions(self):
        """Test the Metadata doesn't throw an error with -rc* versions"""
        Metadata(pdm_version="1.3.3-rc2")

    def test_metadata_with_invalid_pdm_version(self):
        """Test the Metadata throw value exception on invalid version"""
        with pytest.raises(ValidationError):
            Metadata(
                pdm_version="1.X.Y.Z",
            )

    def test_metadata_with_greater_pdm_version(self):
        """Test return value when passing greater pdm_version"""
        metadata = Metadata(
            pdm_version=GREATER_VERSION,
        )
        assert metadata.pdm_version == GREATER_VERSION

    def test_metadata_with_lower_pdm_version(self):
        """Test return value when passing lower pdm_version"""
        with pytest.warns(DeprecationWarning):
            metadata = Metadata(
                pdm_version=LOWER_VERSION,
            )
        assert metadata.pdm_version == LOWER_VERSION

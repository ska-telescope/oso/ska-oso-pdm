from pathlib import Path

from openapi_spec_validator import validate
from openapi_spec_validator.readers import read_from_filename

ROOT = Path(__file__).parents[4].resolve()


def test_openapi_spec_is_valid():
    """
    Tests that the top level oso-components-openapi-v1.yaml spec and all of its
    referenced components are valid against the OpenAPI specification
    """
    # If no exception is raised by when resolving and validating the spec,
    # then it is valid.
    specfile = ROOT / "src/ska_oso_pdm/openapi/oso-components-openapi-v1.yaml"
    spec_dict, base_uri = read_from_filename(specfile)
    file_path = Path(base_uri).parent

    # Fully qualify the schema paths
    for schema_data in spec_dict["components"]["schemas"].values():
        if "$ref" in schema_data:
            schema_data["$ref"] = str(file_path / schema_data["$ref"])

    validate(spec_dict)

from itertools import chain

from .csp import TEST_CASES as csp_cases
from .dish import TEST_CASES as dish_cases
from .execution_block import TEST_CASES as eb_cases
from .mccs import TEST_CASES as mccs_cases
from .project import TEST_CASES as project_cases
from .proposal import TEST_CASES as proposal_cases
from .sb_definition import TEST_CASES as sb_definition_cases
from .sb_instance import TEST_CASES as sb_instance_cases
from .sdp import TEST_CASES as sdp_cases
from .status_history import TEST_CASES as status_history_cases

TEST_CASES = tuple(
    chain(
        csp_cases,
        dish_cases,
        eb_cases,
        mccs_cases,
        project_cases,
        proposal_cases,
        sb_definition_cases,
        sb_instance_cases,
        sdp_cases,
        status_history_cases,
    )
)

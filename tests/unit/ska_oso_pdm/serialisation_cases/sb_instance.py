from datetime import datetime

import pytest

from ska_oso_pdm._shared import FunctionArgs, PythonArguments
from ska_oso_pdm.sb_instance import ActivityCall, Metadata, SBInstance, TelescopeType
from tests.unit.ska_oso_pdm.utils import lbl, load_string_from_file

SB_INSTANCE_JSON = load_string_from_file("testfile_sample_sbi.json")


SAMPLE_DATETIME = datetime.fromisoformat("2022-09-23T15:43:53.971548+00:00")

SB_INSTANCE = SBInstance(
    metadata=Metadata(
        version=1,
        created_by="TestUser",
        created_on=SAMPLE_DATETIME,
        last_modified_by="TestUser",
        last_modified_on=SAMPLE_DATETIME,
        pdm_version="15.3.0",
    ),
    interface="https://schema.skao.int/ska-oso-pdm-sbi/0.1",
    sbi_id="sbi-mvp01-20220923-00001",
    telescope=TelescopeType.SKA_MID,
    sbd_ref="sbd-mvp01-20220923-00001",
    sbd_version=1,
    eb_ref="eb-mvp01-20220923-00001",
    subarray_id=3,
    activities=[
        ActivityCall(
            activity_ref="test activity",
            executed_at=SAMPLE_DATETIME,
            runtime_args=[
                FunctionArgs(
                    function_name="test_fn",
                    function_args=PythonArguments(kwargs=dict(foo="bar")),
                )
            ],
        )
    ],
)


TEST_CASES = [
    pytest.param(SB_INSTANCE_JSON, SB_INSTANCE.model_copy, id=lbl("sb_instance")),
]

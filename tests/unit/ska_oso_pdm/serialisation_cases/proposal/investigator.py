import json
from datetime import datetime

import pytest

from ska_oso_pdm.proposal import Investigator
from tests.unit.ska_oso_pdm.utils import lbl

SAMPLE_DATETIME = datetime.fromisoformat("2022-09-23T15:43:53.971548+00:00")

PROPOSAL_INVESTIGATOR_JSON = {
    "investigator_id": "prp-ska01-202204-01",
    "given_name": "Tony",
    "family_name": "Bennet",
    "email": "somewhere.vague@example.com",
    "organization": "",
    "for_phd": False,
    "status": "pending",
    "principal_investigator": True,
}

PROPOSAL_INVESTIGATOR = Investigator(
    investigator_id="prp-ska01-202204-01",
    given_name="Tony",
    family_name="Bennet",
    email="somewhere.vague@example.com",
    organization="",
    for_phd=False,
    status="pending",
    principal_investigator=True,
)


TEST_CASES = [
    pytest.param(
        json.dumps(PROPOSAL_INVESTIGATOR_JSON),
        PROPOSAL_INVESTIGATOR.model_copy,
        id=lbl("proposal_investigator"),
    )
]

from itertools import chain

from .investigator import TEST_CASES as investigator_test_cases
from .observation_set import TEST_CASES as observation_set_cases
from .proposal import TEST_CASES as proposal_test_cases

TEST_CASES = tuple(
    chain(
        proposal_test_cases,
        investigator_test_cases,
        observation_set_cases,
    )
)

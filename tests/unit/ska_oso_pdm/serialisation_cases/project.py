from datetime import datetime

import pytest

from ska_oso_pdm.project import Author, Metadata, ObservingBlock, Project
from tests.unit.ska_oso_pdm.utils import lbl, load_string_from_file

PROJECT_JSON = load_string_from_file("testfile_sample_project.json")

SAMPLE_DATETIME = datetime.fromisoformat("2022-09-23T15:43:53.971548+00:00")


METADATA = Metadata(
    version=1,
    created_by="TestUser",
    created_on=SAMPLE_DATETIME,
    last_modified_by="TestUser",
    last_modified_on=SAMPLE_DATETIME,
    pdm_version="15.3.0",
)


PROJECT = Project(
    metadata=METADATA,
    interface="https://schema.skao.int/ska-oso-pdm-prj/0.1",
    prj_id="prj-mvp01-20220923-00001",
    name="SKA Mid Project",
    author=Author(pis=["John Lennon"], cois=["Ringo Starr", "George Harrison"]),
    obs_blocks=[
        ObservingBlock(
            obs_block_id="ob-1",
            name="Block 1",
            sbd_ids=[
                "sbd-mvp01-20220923-00001",
                "sbd-mvp01-20220923-00002",
                "sbd-mvp01-20220923-00003",
            ],
        ),
        ObservingBlock(
            obs_block_id="ob-2",
            name="Block 2",
            sbd_ids=[
                "sbd-mvp01-20220923-00004",
                "sbd-mvp01-20220923-00005",
                "sbd-mvp01-20220923-00006",
            ],
        ),
    ],
)

TEST_CASES = [pytest.param(PROJECT_JSON, PROJECT.model_copy, id=lbl("project"))]

"""
tests for the target schema to validate the
conversion between the JSON and Python representations
of the field configuration section of an SKA Scheduling Block
"""

import json

import pytest
from astropy.units import Quantity

from ska_oso_pdm import SpecialCoordinates
from ska_oso_pdm._shared.target import (
    AngleUnits,
    CoordinatesOffset,
    CrossScanParameters,
    EquatorialCoordinates,
    EquatorialCoordinatesPST,
    EquatorialCoordinatesReferenceFrame,
    FivePointParameters,
    GalacticCoordinates,
    HorizontalCoordinates,
    ICRSCoordinates,
    PointedMosaicParameters,
    PointingKind,
    PointingPattern,
    RadialVelocity,
    RadialVelocityDefinition,
    RadialVelocityReferenceFrame,
    RasterParameters,
    SinglePointParameters,
    SolarSystemObject,
    SolarSystemObjectName,
    SpiralParameters,
    StarRasterParameters,
    Target,
)
from tests.unit.ska_oso_pdm.utils import TEST_TARGET_ID, lbl

CROSS_SCAN_PARAMETERS_JSON = {"kind": "CrossScanParameters", "offset_arcsec": 1.23}


def cross_scan_parameters_instance():
    return CrossScanParameters(offset_arcsec=1.23)


FIVE_POINT_PARAMETERS_JSON = {"kind": "FivePointParameters", "offset_arcsec": 1.23}


def five_point_parameters_instance():
    return FivePointParameters(offset_arcsec=1.23)


SINGLE_POINT_PARAMETERS_JSON = {
    "kind": "SinglePointParameters",
    "offset_x_arcsec": 1.23,
    "offset_y_arcsec": 4.56,
}


def single_point_parameters_instance():
    return SinglePointParameters(offset_x_arcsec=1.23, offset_y_arcsec=4.56)


RASTER_PARAMETERS_JSON = {
    "kind": "RasterParameters",
    "row_length_arcsec": 1.23,
    "row_offset_arcsec": 4.56,
    "n_rows": 2,
    "pa": 7.89,
    "unidirectional": True,
}


def raster_parameters_instance():
    return RasterParameters(
        row_length_arcsec=1.23,
        row_offset_arcsec=4.56,
        n_rows=2,
        pa=7.89,
        unidirectional=True,
    )


STAR_RASTER_PARAMETERS_JSON = {
    "kind": "StarRasterParameters",
    "row_length_arcsec": 1.23,
    "n_rows": 2,
    "row_offset_angle": 4.56,
    "unidirectional": True,
}


def spiral_parameters_instance():
    return SpiralParameters(
        scan_extent=Quantity(value=11.0, unit="deg"),
        track_time=Quantity(value=12.0, unit="s"),
        cycle_track_time=Quantity(value=20.0, unit="s"),
        slow_time=Quantity(value=6.5, unit="s"),
        sample_time=Quantity(value=2.25, unit="s"),
        scan_speed=Quantity(value=2.1, unit="deg / s"),
        slew_speed=Quantity(value=-1.4, unit="deg / s"),
        twist_factor=0.5,
        high_el_slowdown_factor=2.1,
    )


SPIRAL_PARAMETERS_JSON = {
    "kind": "SpiralParameters",
    "scan_extent": {"value": 11.0, "unit": "deg"},
    "track_time": {"value": 12.0, "unit": "s"},
    "cycle_track_time": {"value": 20.0, "unit": "s"},
    "slow_time": {"value": 6.5, "unit": "s"},
    "sample_time": {"value": 2.25, "unit": "s"},
    "scan_speed": {"value": 2.1, "unit": "deg / s"},
    "slew_speed": {"value": -1.4, "unit": "deg / s"},
    "twist_factor": 0.5,
    "high_el_slowdown_factor": 2.1,
}


def pointed_mosaic_parameters_instance():
    return PointedMosaicParameters(
        offsets=[
            CoordinatesOffset(x=0.0, y=0.0),
            CoordinatesOffset(x=5.0, y=4.0),
        ],
        units=AngleUnits.ARCSECONDS,
    )


POINTED_MOSAIC_PARAMETERS_JSON = {
    "kind": "PointedMosaicParameters",
    "offsets": [{"x": 0.0, "y": 0.0}, {"x": 5.0, "y": 4.0}],
    "units": "arcsec",
}


def star_raster_parameters_instance():
    return StarRasterParameters(
        row_length_arcsec=1.23, n_rows=2, row_offset_angle=4.56, unidirectional=True
    )


POINTING_PATTERN_JSON = {
    "active": "CrossScanParameters",
    "parameters": [CROSS_SCAN_PARAMETERS_JSON, SINGLE_POINT_PARAMETERS_JSON],
}


def pointing_pattern_instance():
    return PointingPattern(
        active=PointingKind.CROSS_SCAN,
        parameters=[
            cross_scan_parameters_instance(),
            single_point_parameters_instance(),
        ],
    )


POINTING_TEST_CASES = (
    pytest.param(
        json.dumps(FIVE_POINT_PARAMETERS_JSON),
        five_point_parameters_instance,
        id=lbl("fivepoint_parameters"),
    ),
    pytest.param(
        json.dumps(CROSS_SCAN_PARAMETERS_JSON),
        cross_scan_parameters_instance,
        id=lbl("crossscan_parameters"),
    ),
    pytest.param(
        json.dumps(SINGLE_POINT_PARAMETERS_JSON),
        single_point_parameters_instance,
        id=lbl("singlepoint_parameters"),
    ),
    pytest.param(
        json.dumps(RASTER_PARAMETERS_JSON),
        raster_parameters_instance,
        id=lbl("raster_parameters_instance"),
    ),
    pytest.param(
        json.dumps(STAR_RASTER_PARAMETERS_JSON),
        star_raster_parameters_instance,
        id=lbl("star_raster_parameters_instance"),
    ),
    pytest.param(
        json.dumps(SPIRAL_PARAMETERS_JSON),
        spiral_parameters_instance,
        id=lbl("spiral_parameters_instance"),
    ),
    pytest.param(
        json.dumps(POINTING_PATTERN_JSON),
        pointing_pattern_instance,
        id=lbl("crossscan_parameters"),
    ),
    pytest.param(
        json.dumps(POINTED_MOSAIC_PARAMETERS_JSON),
        pointed_mosaic_parameters_instance,
        id=lbl("pointed_mosaic_parameters"),
    ),
)


class RadialVelocityCases:
    CUSTOM_JSON = {
        "quantity": {"value": -12.345, "unit": "m / s"},
        "definition": "OPTICAL",
        "reference_frame": "TOPOCENTRIC",
        "redshift": 1.23,
    }

    DEFAULT_JSON = {
        "quantity": {"value": 0.0, "unit": "km / s"},
        "definition": "RADIO",
        "reference_frame": "LSRK",
        "redshift": 0.0,
    }

    REDSHIFT_ONLY_JSON = {
        "redshift": 1.23,
    }

    QUANTITY_ONLY_JSON = {
        "quantity": {"value": 0.0, "unit": "km / s"},
    }

    @staticmethod
    def custom_instance():
        return RadialVelocity(
            quantity=Quantity(value=-12.345, unit="m/s"),
            definition=RadialVelocityDefinition.OPTICAL,
            reference_frame=RadialVelocityReferenceFrame.TOPOCENTRIC,
            redshift=1.23,
        )

    @staticmethod
    def redshift_only_instance():
        return RadialVelocity(
            redshift=1.23,
        )

    @staticmethod
    def quantity_only_instance():
        return RadialVelocity(
            quantity=Quantity(value=0.0, unit="km / s"),
        )


RADIAL_VELOCITY_TEST_CASES = (
    pytest.param(
        json.dumps(RadialVelocityCases.CUSTOM_JSON),
        RadialVelocityCases.custom_instance,
        id=lbl("radial_velocity.custom"),
    ),
    pytest.param(
        json.dumps(RadialVelocityCases.DEFAULT_JSON),
        RadialVelocity,
        id=lbl("radial_velocity.default"),
    ),
    pytest.param(
        json.dumps(RadialVelocityCases.REDSHIFT_ONLY_JSON),
        RadialVelocityCases.redshift_only_instance,
        marks=[pytest.mark.exported_json_should_differ],
        id=lbl("radial_velocity.redshift_only"),
    ),
    pytest.param(
        json.dumps(RadialVelocityCases.QUANTITY_ONLY_JSON),
        RadialVelocityCases.quantity_only_instance,
        marks=[pytest.mark.exported_json_should_differ],
        id=lbl("radial_velocity.quantity_only"),
    ),
)


EQUATORIALCOORDINATES_JSON = {
    "kind": "equatorial",
    "ra": "12:34:56.78",
    "dec": "-12:34:56.78",
    "reference_frame": "icrs",
    "epoch": 2000.0,
    "unit": ["hourangle", "deg"],
}


def equatorialcoordinates_instance():
    return EquatorialCoordinates(
        ra="12:34:56.78",
        dec="-12:34:56.78",
    )


EQUATORIALCOORDINATESPST_JSON = {
    "target_id": "PSR J0024-7204R",
    "ra_str": "00:24:05.670",
    "dec_str": "-72:04:52.62",
    "reference_frame": "icrs",
    "pm_ra": 0.0,
    "pm_dec": 0.0,
    "parallax": 0.0,
    "epoch": 2000.0,
}


def equatorialcoordinatespst_instance():
    return EquatorialCoordinatesPST(
        target_id="PSR J0024-7204R",
        reference_frame=EquatorialCoordinatesReferenceFrame.ICRS,
        ra_str="00:24:05.670",
        dec_str="-72:04:52.62",
    )


HORIZONTALCOORDINATES_JSON = {
    "kind": "horizontal",
    "az": 12.34,
    "el": 56.78,
    "reference_frame": "altaz",
    "unit": ["deg", "deg"],
}


def horizontalcoordinates_instance():
    return HorizontalCoordinates(az=12.34, el=56.78)


GALACTICCOORDINATES_JSON = {
    "kind": "galactic",
    "l": 12.34,
    "b": 56.78,
    "pm_l": 1.0,
    "pm_b": 2.0,
    "epoch": 2000.0,
    "parallax": 0.0,
}


def galacticcoordinates_instance():
    return GalacticCoordinates(l=12.34, b=56.78, pm_l=1, pm_b=2)


SSO_JSON = {"kind": "sso", "reference_frame": "special", "name": "Venus"}


def sso_instance():
    return SolarSystemObject(name=SolarSystemObjectName.VENUS)


SPECIALCOORDINATES_JSON = {
    "kind": "special",
    "reference_frame": "special",
    "name": "Venus",
}


def specialcoordinates_instance():
    return SpecialCoordinates(name=SolarSystemObjectName.VENUS)


TARGET_JSON = {
    "target_id": TEST_TARGET_ID,
    "name": "foo",
    "pointing_pattern": POINTING_PATTERN_JSON,
    "reference_coordinate": EQUATORIALCOORDINATES_JSON,
    "radial_velocity": RadialVelocityCases.CUSTOM_JSON,
}


def target_instance():
    return Target(
        target_id=TEST_TARGET_ID,
        name="foo",
        pointing_pattern=pointing_pattern_instance(),
        reference_coordinate=equatorialcoordinates_instance(),
        radial_velocity=RadialVelocityCases.custom_instance(),
    )


ICRSCOORDINATES_JSON = {
    "kind": "icrs",
    "reference_frame": "icrs",
    "ra_str": "00:24:05.670",
    "dec_str": "-72:04:52.62",
    "pm_ra": 1.2,
    "pm_dec": 3.4,
    "parallax": 5.6,
    "epoch": 2000.0,
}


def icrscoordinates_instance():
    return ICRSCoordinates(
        reference_frame=EquatorialCoordinatesReferenceFrame.ICRS,
        ra_str="00:24:05.670",
        dec_str="-72:04:52.62",
        pm_ra=1.2,
        pm_dec=3.4,
        parallax=5.6,
    )


COORDINATE_TEST_CASES = (
    pytest.param(
        json.dumps(ICRSCOORDINATES_JSON),
        icrscoordinates_instance,
        id=lbl("ICRSCoordinates"),
    ),
    pytest.param(
        json.dumps(EQUATORIALCOORDINATES_JSON),
        equatorialcoordinates_instance,
        id=lbl("equatorial_coordinates"),
    ),
    pytest.param(
        json.dumps(EQUATORIALCOORDINATESPST_JSON),
        equatorialcoordinatespst_instance,
        id=lbl("equatorial_pst_coordinates"),
    ),
    pytest.param(
        json.dumps(HORIZONTALCOORDINATES_JSON),
        horizontalcoordinates_instance,
        id=lbl("horizontal_coordinates"),
    ),
    pytest.param(
        json.dumps(GALACTICCOORDINATES_JSON),
        galacticcoordinates_instance,
        id=lbl("galactic_coordinates"),
    ),
    pytest.param(json.dumps(SSO_JSON), sso_instance, id=lbl("solar_system_object")),
    pytest.param(
        json.dumps(SPECIALCOORDINATES_JSON),
        specialcoordinates_instance,
        id=lbl("SpecialCoordinates"),
    ),
    pytest.param(json.dumps(TARGET_JSON), target_instance, id=lbl("target")),
)


TEST_CASES = POINTING_TEST_CASES + COORDINATE_TEST_CASES + RADIAL_VELOCITY_TEST_CASES

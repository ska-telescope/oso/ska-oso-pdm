"""
tests for the processing_block schema to validate the
conversion between the JSON and Python representations
of the processing block section of an SDP configuration
"""

import pytest

from ska_oso_pdm.sb_definition.sdp.processing_block import (
    PbDependency,
    ProcessingBlock,
    Script,
    ScriptKind,
)
from tests.unit.ska_oso_pdm.utils import lbl

VALID_PROCESSING_BLOCK_JSON = """
{
  "pb_id": "pb-mvp01-20200325-00003",
  "sbi_refs": ["sbi-mvp01-20200325-00002"],
  "script": {
    "version": "0.1.0",
    "name": "ical",
    "kind": "batch"
    },
  "dependencies": [
    {
      "pb_ref": "pb-mvp01-20200325-00001",
      "kind": ["visibilities"]
    }
  ]
}
"""


VALID_PROCESSING_BLOCK_NO_DEP_JSON = """
{
  "pb_id": "pb-mvp01-20200325-00003",
  "sbi_refs": ["sbi-mvp01-20200325-00002"],
  "script": {
    "version": "0.1.0",
    "name": "ical",
    "kind": "batch"
    }
}
"""
VALID_PROCESSING_BLOCK_DEPENDENCY_JSON = """
{
  "pb_ref": "pb-mvp01-20200325-00001",
  "kind": ["visibilities"]
}
"""
VALID_SCRIPT_JSON = '{"kind": "batch", "name": "ical", "version": "0.1.0"}'

SCRIPT = Script(name="ical", kind=ScriptKind.BATCH, version="0.1.0")
DEP = PbDependency(pb_ref="pb-mvp01-20200325-00001", kind=["visibilities"])

VALID_PROCESSING_BLOCK = ProcessingBlock(
    pb_id="pb-mvp01-20200325-00003",
    script=SCRIPT,
    sbi_refs=["sbi-mvp01-20200325-00002"],
    parameters={},
    dependencies=[DEP],
)

PROCESSING_BLOCK_NO_DEPS = ProcessingBlock(
    pb_id="pb-mvp01-20200325-00003",
    script=SCRIPT,
    parameters={},
    sbi_refs=["sbi-mvp01-20200325-00002"],
)

TEST_CASES = (
    pytest.param(
        VALID_PROCESSING_BLOCK_JSON,
        VALID_PROCESSING_BLOCK.model_copy,
        id=lbl("processing_block"),
    ),
    pytest.param(
        VALID_PROCESSING_BLOCK_NO_DEP_JSON,
        PROCESSING_BLOCK_NO_DEPS.model_copy,
        id=lbl("processing_block_without_deps"),
    ),
    pytest.param(VALID_SCRIPT_JSON, SCRIPT.model_copy, id=lbl("script")),
    pytest.param(
        VALID_PROCESSING_BLOCK_DEPENDENCY_JSON, DEP.model_copy, id=lbl("pb_dependency")
    ),
)

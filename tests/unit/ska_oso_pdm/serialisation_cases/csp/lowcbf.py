"""
tests for the LOW.CBF configuration schema to validate the
conversion between the JSON and Python representations
of the csp configuration section of an SKA Scheduling Block
"""

import json
from functools import partial

import pytest

from ska_oso_pdm.sb_definition.csp.lowcbf import Correlation, LowCBFConfiguration
from tests.unit.ska_oso_pdm.utils import lbl

CorrelationFactory = partial(
    Correlation,
    spw_id=1,
    logical_fsp_ids=[0, 1],
    zoom_factor=0,
    centre_frequency=199.609375e6,
    number_of_channels=96,
    integration_time_ms=849,
)

CORRELATION_JSON = {
    "spw_id": 1,
    "logical_fsp_ids": [0, 1],
    "zoom_factor": 0,
    "centre_frequency": 199.609375e6,
    "number_of_channels": 96,
    "integration_time_ms": 849,
}

LowCBFConfigurationFactory = partial(
    LowCBFConfiguration, do_pst=True, correlation_spws=[CorrelationFactory()]
)


def low_cbf_no_pst() -> LowCBFConfiguration:
    return LowCBFConfiguration(correlation_spws=[CorrelationFactory()])


LOW_CBF_CONFIG_NO_PST_JSON = {
    "do_pst": False,
    "correlation_spws": [
        {
            "spw_id": 1,
            "logical_fsp_ids": [0, 1],
            "zoom_factor": 0,
            "centre_frequency": 199.609375e6,
            "number_of_channels": 96,
            "integration_time_ms": 849,
        }
    ],
}

LOW_CBF_CONFIG_WITH_PST_JSON = {
    "do_pst": True,
    "correlation_spws": [
        {
            "spw_id": 1,
            "logical_fsp_ids": [0, 1],
            "zoom_factor": 0,
            "centre_frequency": 199.609375e6,
            "number_of_channels": 96,
            "integration_time_ms": 849,
        }
    ],
}

TEST_CASES = (
    pytest.param(
        json.dumps(LOW_CBF_CONFIG_NO_PST_JSON),
        low_cbf_no_pst,
        id=lbl("station_configuration_no_pst"),
    ),
    pytest.param(
        json.dumps(LOW_CBF_CONFIG_WITH_PST_JSON),
        LowCBFConfigurationFactory,
        id=lbl("station_configuration"),
    ),
)
